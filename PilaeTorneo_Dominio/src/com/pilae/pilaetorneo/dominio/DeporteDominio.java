package com.pilae.pilaetorneo.dominio;

import static com.pilae.pilaetorneo.utilitarios.cadenas.UtilTexto.obtenerUtilTexto;

import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;

public final class DeporteDominio {
	
	private int idDeporte;
	private String nombre;
	private String descripcion;
	private OperacionEnum operacion;
	
	private DeporteDominio(final int idDeporte, final String nombre, final String descripcion, final OperacionEnum operacion) {
		super();
		setIdDeporte(idDeporte);
		setNombre(nombre);
		setDescripcion(descripcion);
		setOperacion(operacion);
	}
	
	public static DeporteDominio crear(final int idDeporte, final String nombre, final String descripcion, final OperacionEnum operacion) {
				
		final DeporteDominio retorno = new DeporteDominio(idDeporte, nombre, descripcion, operacion);
				
		switch (retorno.getOperacion()) {
		
		case CREAR:
			retorno.asegurarIntegridadNombre();
			retorno.asegurarIntegridadDescripcion();
			break;
			
		case POBLAR:
		case ACTUALIZAR:
			retorno.asegurarIntegridadIdDeporte();
			retorno.asegurarIntegridadNombre();
			retorno.asegurarIntegridadDescripcion();
			break;
			
		case DEPENDENCIA:	
		case ELIMINAR:
			retorno.asegurarIntegridadIdDeporte();
			break;
		case CONSULTAR:
			
			if(retorno.getIdDeporte() > 0) {
				retorno.asegurarIntegridadIdDeporte();
			}
			if(!obtenerUtilTexto().cadenaEsVaciaONula(retorno.getNombre())) {
				retorno.asegurarIntegridadNombre();
			}
			if(!obtenerUtilTexto().cadenaEsVaciaONula(retorno.getDescripcion())) {
				retorno.asegurarIntegridadDescripcion();
			}
			break;
		
		default:	
			String mensaje = "El Objeto Deporte no se puede manejar, debido a que la operaci�n para validar su integridad no existe ";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
		
		return retorno;
	}

	public final int getIdDeporte() {
		return idDeporte;
	}

	public final String getNombre() {
		return nombre;
	}
	
	public final String getDescripcion() {
		return descripcion;
	}
	
	public final OperacionEnum getOperacion() {
		return operacion;
	}

	private final void setIdDeporte(final int idDeporte) {
		this.idDeporte = idDeporte;
	}

	private final void setNombre(final String nombre) {
		this.nombre = obtenerUtilTexto().aplicarTrim(nombre);
	}

	private final void setDescripcion(final String descripcion) {
		this.descripcion = obtenerUtilTexto().aplicarTrim(descripcion);
	}
	
	private final void setOperacion(final OperacionEnum operacion) {
		this.operacion = operacion;
	}

	private void asegurarIntegridadIdDeporte() {
		if(getIdDeporte() <= 0) {
			String mensaje = "El id_Deporte de un Deporte debe ser mayor a cero";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
	}
	
	private void asegurarIntegridadNombre() {
		if(obtenerUtilTexto().cadenaEsVaciaONula(getNombre())) {
			String mensaje = "El nombre de un Deporte no puede ser null, ni vac�a";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		} else if (getNombre().trim().length() > 50) {
			String mensaje = "El nombre de un Deporte no puede tener m�s de 50 caracteres";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		} else if (getNombre().trim().length() < 3) {
			String mensaje = "El nombre de un Deporte no puede tener menos de 3 caracteres";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
	}
	
	private void asegurarIntegridadDescripcion() {
		if (getDescripcion().trim().length() > 200) {
			String mensaje = "El descripcion de un Deporte no puede superar los 200 caracteres";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
	}
	
}