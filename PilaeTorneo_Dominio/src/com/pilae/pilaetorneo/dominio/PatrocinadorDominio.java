package com.pilae.pilaetorneo.dominio;

import static com.pilae.pilaetorneo.utilitarios.cadenas.UtilTexto.obtenerUtilTexto;

import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;

public class PatrocinadorDominio {
	
	private int idPatrocinador;
	private String nombre;
	private String descripcion;
	private OperacionEnum operacion;
	
	private PatrocinadorDominio(final int idPatrocinador, final String nombre, final String descripcion, final OperacionEnum operacion) {
		super();
		setIdPatrocinador(idPatrocinador);
		setNombre(nombre);
		setDescripcion(descripcion);
		setOperacion(operacion);
	}
	
	public static PatrocinadorDominio crear(final int idPatrocinador, final String nombre, final String descripcion, final OperacionEnum operacion) {
		
		final PatrocinadorDominio retorno = new PatrocinadorDominio(idPatrocinador, nombre, descripcion, operacion);
				
		switch (retorno.getOperacion()) {
		
		case CREAR:
			retorno.asegurarIntegridadNombre();
			retorno.asegurarIntegridadDescripcion();
			break;
			
		case POBLAR:
		case ACTUALIZAR:
			retorno.asegurarIntegridadIdPatrocinador();
			retorno.asegurarIntegridadNombre();
			retorno.asegurarIntegridadDescripcion();
			break;
			
		case DEPENDENCIA:	
		case ELIMINAR:
			retorno.asegurarIntegridadIdPatrocinador();
			break;
		case CONSULTAR:
			
			if(retorno.getIdPatrocinador() > 0) {
				retorno.asegurarIntegridadIdPatrocinador();
			}
			if(!obtenerUtilTexto().cadenaEsVaciaONula(retorno.getNombre())) {
				retorno.asegurarIntegridadNombre();
			}
			if(!obtenerUtilTexto().cadenaEsVaciaONula(retorno.getDescripcion())) {
				retorno.asegurarIntegridadDescripcion();
			}
			break;
		
		default:	
			String mensaje = "El Objeto Patrocinador no se puede manejar, debido a que la operaci�n para validar su integridad no existe.";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
		
		return retorno;
	}

	private void asegurarIntegridadIdPatrocinador() {
		if(getIdPatrocinador() <= 0) {
			String mensaje = "El id_Patrocinador de un Patrocinador debe ser mayor a cero";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
	}

	private void asegurarIntegridadNombre() {
		if(obtenerUtilTexto().cadenaEsVaciaONula(getNombre())) {
			String mensaje = "El nombre de un Patrocinador no puede ser null, ni vac�a";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		} else if (getNombre().trim().length() > 50) {
			String mensaje = "El nombre de un Patrocinador no puede tener m�s de 50 caracteres";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}	
	}
	
	private void asegurarIntegridadDescripcion() {
		if(getDescripcion() == null) {
			String mensaje = "La descripci�n de un Patrocinador no puede ser null";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		} else if (getDescripcion().trim().length() > 200) {
			String mensaje = "La descripcion de un Patrocinador no puede superar los 200 caracteres";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
	}

	public final int getIdPatrocinador() {
		return idPatrocinador;
	}

	public final String getNombre() {
		return nombre;
	}

	public final String getDescripcion() {
		return descripcion;
	}

	private final void setIdPatrocinador(int idPatrocinador) {
		this.idPatrocinador = idPatrocinador;
	}

	private final void setNombre(String nombre) {
		this.nombre = nombre;
	}

	private final void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public final OperacionEnum getOperacion() {
		return operacion;
	}

	private final void setOperacion(OperacionEnum operacion) {
		this.operacion = operacion;
	}
	
	
}
