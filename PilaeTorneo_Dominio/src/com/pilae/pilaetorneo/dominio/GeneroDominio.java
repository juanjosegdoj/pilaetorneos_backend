package com.pilae.pilaetorneo.dominio;

import static com.pilae.pilaetorneo.utilitarios.cadenas.UtilTexto.obtenerUtilTexto;

import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;

public class GeneroDominio {

	private int idGenero;
	private String nombre;
	private OperacionEnum operacion;
	
	private GeneroDominio(final int idGenero, final String nombre, final OperacionEnum operacion) {
		super();
		setIdGenero(idGenero);
		setNombre(nombre);
		setOperacion(operacion);
	}
	
	public static GeneroDominio crear(final int idGenero, final String nombre, final OperacionEnum operacion) {
		final GeneroDominio retorno = new GeneroDominio(idGenero, nombre, operacion);
		
		switch (retorno.getOperacion()) {
		
		case CREAR:
			retorno.asegurarIntegridadNombre();
			break;
			
		case POBLAR:
		case ACTUALIZAR:
			retorno.asegurarIntegridadIdGenero();
			retorno.asegurarIntegridadNombre();
			break;
			
		case DEPENDENCIA:	
		case ELIMINAR:
			retorno.asegurarIntegridadIdGenero();
			break;
		case CONSULTAR:
			if(retorno.getIdGenero() > 0) {
				retorno.asegurarIntegridadIdGenero();
			}
			if(!obtenerUtilTexto().cadenaEsVaciaONula(retorno.getNombre())) {
				retorno.asegurarIntegridadNombre();
			}
			break;
		
		default:	
			String mensaje = "El Objeto Genero no se puede manejar, debido a que la operación para validar su integridad no existe.";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
		
		return retorno;
	}

	public final int getIdGenero() {
		return idGenero;
	}
	public final String getNombre() {
		return nombre;
	}
	public final OperacionEnum getOperacion() {
		return operacion;
	}
	private final void setIdGenero(int idGenero) {
		this.idGenero = idGenero;
	}
	private final void setNombre(String nombre) {
		this.nombre = nombre;
	}
	private final void setOperacion(OperacionEnum operacion) {
		this.operacion = operacion;
	}
	
	private void asegurarIntegridadNombre() {
		if(getNombre() == null) {
			String mensaje = "El nombre de un Genero no puede ser null";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		} else if (getNombre().trim().length() > 30) {
			String mensaje = "El nombre de un Genero no puede superar los 30 caracteres";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
	}
	
	private void asegurarIntegridadIdGenero() {
		if(getIdGenero() <= 0) {
			String mensaje = "El id_Genero de un Genero debe ser mayor a cero";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
	}
}
