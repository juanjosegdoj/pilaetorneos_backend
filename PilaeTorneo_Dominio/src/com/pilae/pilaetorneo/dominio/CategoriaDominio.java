package com.pilae.pilaetorneo.dominio;

import static com.pilae.pilaetorneo.utilitarios.cadenas.UtilTexto.obtenerUtilTexto;
import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;

import java.util.Date;
import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import com.pilae.pilaetorneo.utilitarios.fecha.UtilFecha;

public class CategoriaDominio {
	
	private int idCategoria;
	private String nombre;
	private Date fechaNacimientoMaxima;
	private Date fechaNacimientoMinimo;
	private DeporteDominio deporte;
	private int anio;
	private OperacionEnum operacion;
	
	private CategoriaDominio(final int idCategoria, final String nombre, Date fechaNacimientoMaxima, Date fechaNacimientoMinimo,
			final DeporteDominio deporte, final int anio, final OperacionEnum operacion) {
		super();
		
		setIdCategoria(idCategoria);
		setNombre(nombre);
		setFechaNacimientoMaxima(fechaNacimientoMaxima);
		setFechaNacimientoMinimo(fechaNacimientoMinimo);
		setDeporte(deporte);
		setAnio(anio);
		setOperacion(operacion);
	}

	public static CategoriaDominio crear(final int idCategoria, final String nombre, final Date fechaNacimientoMaxima, final Date fechaNacimientoMinimo,
			final DeporteDominio deporte, final int anio, final OperacionEnum operacion) {
		
		final CategoriaDominio retorno = new CategoriaDominio(idCategoria, nombre, fechaNacimientoMaxima, fechaNacimientoMinimo, deporte, anio, operacion);
		
		switch (retorno.getOperacion()) {
		case CREAR:
			retorno.asegurarIntegridadNombre();
			retorno.asegurarIntegridadFechaNacimientoMaxima();
			retorno.asegurarIntegridadNacimientoMinimo();
			retorno.asegurarIntegridadDeporte();
			retorno.asegurarIntegridadAnio();
		
			break;	
			
		case POBLAR:
		case ACTUALIZAR:
			
			retorno.asegurarIntegridadIdCategoria();
			retorno.asegurarIntegridadNombre();
			retorno.asegurarIntegridadFechaNacimientoMaxima();
			retorno.asegurarIntegridadNacimientoMinimo();
			retorno.asegurarIntegridadDeporte();
			retorno.asegurarIntegridadAnio();
			  
			break;

		case DEPENDENCIA:
		case ELIMINAR:
			retorno.asegurarIntegridadIdCategoria();
			break;
			
		case CONSULTAR:
			if(retorno.getIdCategoria() > 0) {
				retorno.asegurarIntegridadIdCategoria();
			}
			if(!obtenerUtilTexto().cadenaEsVaciaONula(retorno.getNombre())) {
				retorno.asegurarIntegridadNombre();
			}
			if(!obtenerUtilObjeto().objetoEsNulo(retorno.getFechaNacimientoMaxima())) {
				retorno.asegurarIntegridadFechaNacimientoMaxima();
			}
			if(!obtenerUtilObjeto().objetoEsNulo(retorno.getFechaNacimientoMinimo())) {
				retorno.asegurarIntegridadNacimientoMinimo();
			}
			if(retorno.getAnio() > 0) {
				retorno.asegurarIntegridadAnio();
			}
			break;
		default:
			String mensaje = "El Objeto Categoria no se puede manejar, debido a que la operaci�n para validar su integridad no existe ";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
		return retorno;
	}
	
	private void asegurarIntegridadIdCategoria() {
		if(getIdCategoria() <= 0) {
			String mensaje = "El id_Categoria de una Categoria debe ser mayor a 0";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
		
	}

	private void asegurarIntegridadAnio() {
		if(getAnio() <= 0) {
			String mensaje = "El A�o debe ser mayor a 0";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
	}

	private void asegurarIntegridadDeporte() {
		
		if(obtenerUtilObjeto().objetoEsNulo(getDeporte())) {
			String mensajeTecnico = "La categoria obligatoriamente debe tener un deporte.";
			String mensajeUsuario = "El Deporte de una categoria no puede ser nulo.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.DOMINIO);
		}
		if(getDeporte().getIdDeporte() < 0) {
			String mensajeTecnico = "El id_deporte de un Deporte debe ser mayor a cero para poderlo ligarlo con Categoria.";
			String mensajeUsuario = "No se puede asociar la Categoria ingresada con el deporte ingresado.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.DOMINIO);
		}
		if(!obtenerUtilObjeto().objetoEsNulo(getDeporte().getNombre()) && getDeporte().getNombre().trim().length() > 50) {
			String mensaje = "El nombre del deporte debe ser menor o igual a 50 catacteres de longitud";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
	}

	private void asegurarIntegridadNacimientoMinimo() {
		
		System.out.println(getFechaNacimientoMinimo());
		
		if(!obtenerUtilObjeto().objetoEsNulo(getFechaNacimientoMinimo()) && getFechaNacimientoMinimo().after(UtilFecha.obtenerUtilFecha().fechaActual())) {		
			String mensaje = "La fecha de nacimiento m�nima de una Categoria no puede superar la fecha actual."; 
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
	}

	private void asegurarIntegridadFechaNacimientoMaxima() {
		
		if(obtenerUtilObjeto().objetoEsNulo(getFechaNacimientoMaxima())) {		
			String mensaje = "La fecha de nacimiento M�xima no puede ser nulo.";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
		if(getFechaNacimientoMaxima().after(UtilFecha.obtenerUtilFecha().fechaActual())) {
			String mensaje = "La fecha de nacimiento m�xima de una Categoria no puede superar la fecha actual."; 
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
		if(!obtenerUtilObjeto().objetoEsNulo(getFechaNacimientoMinimo()) && getFechaNacimientoMinimo().before(getFechaNacimientoMaxima())) {
			String mensaje = "La fecha de nacimiento m�xima debe ser m�s vieja que la fecha de nacimineto m�nima";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
	}

	private void asegurarIntegridadNombre() {
		
		if(obtenerUtilTexto().cadenaEsVaciaONula(getNombre())) {
			String mensaje = "El nombre de una Categoria no puede ser null, ni vac�a";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		} else if (getNombre().trim().length() > 50) {
			String mensaje = "El nombre de una Categoria no puede tener m�s de 50 caracteres";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		} else if (getNombre().trim().length() < 3) {
			String mensaje = "El nombre de una Categoria no puede tener menos de 3 caracteres";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
		
	}

	public final int getIdCategoria() {
		return idCategoria;
	}

	public final String getNombre() {
		return nombre;
	}

	public final Date getFechaNacimientoMaxima() {
		return fechaNacimientoMaxima;
	}

	public final Date getFechaNacimientoMinimo() {
		return fechaNacimientoMinimo;
	}

	public final DeporteDominio getDeporte() {
		return deporte;
	}

	public final int getAnio() {
		return anio;
	}

	public final OperacionEnum getOperacion() {
		return operacion;
	}

	private final void setIdCategoria(final int idCategoria) {
		this.idCategoria = idCategoria;
	}

	private final void setNombre(final String nombre) {
		this.nombre = nombre;
	}
	
	private final void setFechaNacimientoMaxima(final Date fechaNacimientoMaxima) {
		this.fechaNacimientoMaxima = fechaNacimientoMaxima;
	}

	private final void setFechaNacimientoMinimo(final Date fechaNacimientoMinimo) {
		this.fechaNacimientoMinimo = fechaNacimientoMinimo;
	}

	private final void setDeporte(final DeporteDominio deporte) {
		this.deporte = deporte;
	}

	private final void setAnio(final int anio) {
		this.anio = anio;
	}

	private final void setOperacion(final OperacionEnum operacion) {
		this.operacion = operacion;
	}
	
	
	
}
