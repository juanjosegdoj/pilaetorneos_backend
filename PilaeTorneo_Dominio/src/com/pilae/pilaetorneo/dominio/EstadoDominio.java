package com.pilae.pilaetorneo.dominio;

import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import static com.pilae.pilaetorneo.utilitarios.cadenas.UtilTexto.obtenerUtilTexto;

public class EstadoDominio {
	
	private int idEstado;
	private String nombre;
	private String descripcion;
	private OperacionEnum operacion;
	
	private EstadoDominio(final int idEstado, final String nombre, final String descripcion, final OperacionEnum operacion) {
		super();
		setIdEstado(idEstado);
		setNombre(nombre);
		setDescripcion(descripcion);
		setOperacion(operacion);
	}
	
	public static EstadoDominio crear(final int idEstado, final String nombre, final String descripcion, OperacionEnum operacion) {
		 
		EstadoDominio retorno = new EstadoDominio(idEstado, nombre, descripcion, operacion);
		
		switch (retorno.getOperacion()) {
		case CREAR:
			retorno.asegurarIntegridadNombre();
			retorno.asegurarIntegridadDescripcion();
			break;
			
		case POBLAR:
		case ACTUALIZAR:
			retorno.asegurarIntegridadIdEstado();
			retorno.asegurarIntegridadNombre();
			retorno.asegurarIntegridadDescripcion();
			break;
			
		case DEPENDENCIA:
		case ELIMINAR:
			retorno.asegurarIntegridadIdEstado();
			break;
			
		case CONSULTAR:
			if(retorno.getIdEstado() > 0) {
				retorno.asegurarIntegridadIdEstado();
			}
			if(!obtenerUtilTexto().cadenaEsVaciaONula(retorno.getNombre())) {
				retorno.asegurarIntegridadNombre();
			}
			if(!obtenerUtilTexto().cadenaEsVaciaONula(retorno.getDescripcion())) {
				retorno.asegurarIntegridadDescripcion();
			}
			break;
			
		default:
			String mensaje = "El Objeto Estado no se puede manejar de ninguna manera, debido a que la operaci�n para validar su integridad no existe.";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
		
		return retorno; 
	}
	
	public final int getIdEstado() {
		return idEstado;
	}
	public final String getNombre() {
		return nombre;
	}
	public final String getDescripcion() {
		return descripcion;
	}
	public final OperacionEnum getOperacion() {
		return operacion;
	}
	
	private final void setIdEstado(final int idEstado) {
		this.idEstado = idEstado;
	}
	private final void setNombre(final String nombre) {
		this.nombre = nombre;
	}
	private final void setDescripcion(final String descripcion) {
		this.descripcion = descripcion;
	}
	private final void setOperacion(final OperacionEnum operacion) {
		this.operacion = operacion;
	}
	
	private void asegurarIntegridadIdEstado() {
		if(getIdEstado() <= 0) {
			String mensaje = "El id_Estado de un Estado debe ser mayor a cero";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
	}
	
	private void asegurarIntegridadNombre() {
		if(obtenerUtilTexto().cadenaEsVaciaONula(getNombre())) {
			String mensaje = "El nombre de un Estado no puede ser null, ni vac�o";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		} else if (getNombre().trim().length() > 50) {
			String mensaje = "El nombre de un Estado no puede tener m�s de 50 caracteres";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
	}
	
	private void asegurarIntegridadDescripcion() {
		if(obtenerUtilTexto().cadenaEsVaciaONula(getNombre())) {
			String mensaje = "La descripci�n de un Estado no puede ser null, n� estar vac�a.";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		} else if (getDescripcion().trim().length() > 200) {
			String mensaje = "La descripcion de un Estado no puede superar los 200 caracteres";
			throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
		}
	}
}
