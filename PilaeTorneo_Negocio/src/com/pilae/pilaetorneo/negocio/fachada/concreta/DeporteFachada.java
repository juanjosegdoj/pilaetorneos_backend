package com.pilae.pilaetorneo.negocio.fachada.concreta;

import java.util.List;

import com.pilae.pilaeTorneo.datos.factoria.abstracta.FactoriaDAO;
import com.pilae.pilaetorneo.dto.DeporteDTO;
import com.pilae.pilaetorneo.negocio.fachada.IDeporteFachada;
import com.pilae.pilaetorneo.negocio.negocio.IDeporteNegocio;
import com.pilae.pilaetorneo.negocio.negocio.concreta.DeporteNegocio;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import com.pilae.pilaetorneo.utilitarios.sql.enumeracion.FuenteInformacionEnum;

public final class DeporteFachada implements IDeporteFachada {

	@Override
	public final void crear(DeporteDTO deporte) {
		
		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		
		try {
			factoria.iniciarTransaccion();
			
			final IDeporteNegocio negocio = new DeporteNegocio(factoria);
			negocio.crear(deporte);
			
			factoria.confirmarTransaccion();
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de registrar la información del nuevo Deporte";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de crear el nuevo Deporte";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			factoria.cerrarConexion();
		}
	}

	@Override
	public final void actualizar(DeporteDTO deporte) {
		
		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		
		try {
			factoria.iniciarTransaccion();
			
			final IDeporteNegocio negocio = new DeporteNegocio(factoria);
			negocio.actualizar(deporte);
			
			factoria.confirmarTransaccion();
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de actualizar la información del Deporte";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de actualizar la información de Deporte";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			factoria.cerrarConexion();
		}
		
	}

	@Override
	public final void eliminar(DeporteDTO deporte) {
		
		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		
		try {
			factoria.iniciarTransaccion();
			
			final IDeporteNegocio negocio = new DeporteNegocio(factoria);
			negocio.eliminar(deporte);
			
			factoria.confirmarTransaccion();
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de eliminar la información del Deporte";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de eliminar el Deporte.";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			factoria.cerrarConexion();
		}
	}

	@Override
	public final List<DeporteDTO> consultar(DeporteDTO deporte) {
		
		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		List<DeporteDTO> deportes;
		
		try {
			factoria.iniciarTransaccion();
			
			final IDeporteNegocio negocio = new DeporteNegocio(factoria);

			deportes = negocio.consultar(deporte);
			
			factoria.confirmarTransaccion();
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de consultar la información del nuevo Deporte";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de consultar el nuevo Deporte.";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			factoria.cerrarConexion();
		}
		
		return deportes;
	}
}




