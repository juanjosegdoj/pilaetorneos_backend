package com.pilae.pilaetorneo.negocio.fachada.concreta;

import java.util.List;

import com.pilae.pilaeTorneo.datos.factoria.abstracta.FactoriaDAO;
import com.pilae.pilaetorneo.dto.EstadoDTO;
import com.pilae.pilaetorneo.negocio.fachada.IEstadoFachada;
import com.pilae.pilaetorneo.negocio.negocio.IEstadoNegocio;
import com.pilae.pilaetorneo.negocio.negocio.concreta.EstadoNegocio;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import com.pilae.pilaetorneo.utilitarios.sql.enumeracion.FuenteInformacionEnum;

public class EstadoFachada implements IEstadoFachada {
	
	@Override
	public final void crear(EstadoDTO estado) {
		
		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		
		try {
			factoria.iniciarTransaccion();
			
			final IEstadoNegocio negocio = new EstadoNegocio(factoria);
			negocio.crear(estado);
			
			factoria.confirmarTransaccion();
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de registrar la información del nuevo Estado";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de crear el nuevo Estado";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			factoria.cerrarConexion();
		}
	}

	@Override
	public final void actualizar(EstadoDTO estado) {
		
		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		
		try {
			factoria.iniciarTransaccion();
			
			final IEstadoNegocio negocio = new EstadoNegocio(factoria);
			negocio.actualizar(estado);
			
			factoria.confirmarTransaccion();
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de actualizar la información del nuevo Estado";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de actualizar la información del Estado";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			factoria.cerrarConexion();
		}
		
	}

	@Override
	public final void eliminar(EstadoDTO estado) {
		
		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		
		try {
			factoria.iniciarTransaccion();
			
			final IEstadoNegocio negocio = new EstadoNegocio(factoria);
			negocio.eliminar(estado);
			
			factoria.confirmarTransaccion();
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de eliminar la información del nuevo Estado";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de eliminar el Estado.";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			factoria.cerrarConexion();
		}
	}

	@Override
	public final List<EstadoDTO> consultar(EstadoDTO deporte) {
		
		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		List<EstadoDTO> deportes;
		
		try {
			factoria.iniciarTransaccion();
			
			final IEstadoNegocio negocio = new EstadoNegocio(factoria);
			deportes = negocio.consultar(deporte);
			
			factoria.confirmarTransaccion();
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de consultar la información del Estado";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de consultar el Estado.";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			factoria.cerrarConexion();
		}
		
		return deportes;
	}

}
