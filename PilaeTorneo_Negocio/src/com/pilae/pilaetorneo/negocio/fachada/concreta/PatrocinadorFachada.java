package com.pilae.pilaetorneo.negocio.fachada.concreta;

import java.util.List;

import com.pilae.pilaeTorneo.datos.factoria.abstracta.FactoriaDAO;
import com.pilae.pilaetorneo.dto.PatrocinadorDTO;
import com.pilae.pilaetorneo.negocio.fachada.IPatrocinadorFachada;
import com.pilae.pilaetorneo.negocio.negocio.IPatrocinadorNegocio;
import com.pilae.pilaetorneo.negocio.negocio.concreta.PatrocinadorNegocio;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import com.pilae.pilaetorneo.utilitarios.sql.enumeracion.FuenteInformacionEnum;

public class PatrocinadorFachada implements IPatrocinadorFachada{
	
	@Override
	public final void crear(PatrocinadorDTO patrocinador) {
		
		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		
		try {
			factoria.iniciarTransaccion();
			
			final IPatrocinadorNegocio negocio = new PatrocinadorNegocio(factoria);
			negocio.crear(patrocinador);
			
			factoria.confirmarTransaccion();
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de registrar la información del nuevo patrocinador";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de crear el nuevo patrocinador";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			factoria.cerrarConexion();
		}
	}

	@Override
	public void actualizar(PatrocinadorDTO patrocinador) {
		
		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		
		try {
			factoria.iniciarTransaccion();
			
			final IPatrocinadorNegocio negocio = new PatrocinadorNegocio(factoria);
			negocio.actualizar(patrocinador);
			
			factoria.confirmarTransaccion();
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de actualizar la información el patrocinador";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de actualizar el patrocinador";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			factoria.cerrarConexion();
		}
	}

	@Override
	public void eliminar(PatrocinadorDTO patrocinador) {
		
		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		
		try {
			factoria.iniciarTransaccion();
			
			final IPatrocinadorNegocio negocio = new PatrocinadorNegocio(factoria);
			negocio.eliminar(patrocinador);
			
			factoria.confirmarTransaccion();
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de eliminar la información del patrocinador";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de actualizar el patrocinador";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			factoria.cerrarConexion();
		}
	}

	@Override
	public List<PatrocinadorDTO> consultar(PatrocinadorDTO patrocinador) {
		
		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		
		List<PatrocinadorDTO> patrocinadores;
		
		try {
			factoria.iniciarTransaccion();
			
			final IPatrocinadorNegocio negocio = new PatrocinadorNegocio(factoria);
			patrocinadores = negocio.consultar(patrocinador);
			
			factoria.confirmarTransaccion();
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de consultar la información del patrocinador";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de consultar el patrocinador";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			factoria.cerrarConexion();
		}
		
		return patrocinadores;
	}
}
