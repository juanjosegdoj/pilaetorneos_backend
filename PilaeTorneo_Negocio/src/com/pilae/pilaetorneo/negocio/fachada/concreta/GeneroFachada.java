package com.pilae.pilaetorneo.negocio.fachada.concreta;

import java.util.List;

import com.pilae.pilaeTorneo.datos.factoria.abstracta.FactoriaDAO;
import com.pilae.pilaetorneo.dto.GeneroDTO;
import com.pilae.pilaetorneo.negocio.fachada.IGeneroFachada;
import com.pilae.pilaetorneo.negocio.negocio.IGeneroNegocio;
import com.pilae.pilaetorneo.negocio.negocio.concreta.GeneroNegocio;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import com.pilae.pilaetorneo.utilitarios.sql.enumeracion.FuenteInformacionEnum;

public class GeneroFachada implements IGeneroFachada{
	
	@Override
	public final void crear(GeneroDTO genero) {
		
		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		
		try {
			factoria.iniciarTransaccion();
			
			final IGeneroNegocio negocio = new GeneroNegocio(factoria);
			negocio.crear(genero);
			
			factoria.confirmarTransaccion();
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de registrar la información del nuevo Genero";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de crear el nuevo Genero";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			factoria.cerrarConexion();
		}
	}

	@Override
	public void actualizar(GeneroDTO genero) {
		
		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		
		try {
			factoria.iniciarTransaccion();
			
			final IGeneroNegocio negocio = new GeneroNegocio(factoria);
			negocio.actualizar(genero);
			
			factoria.confirmarTransaccion();
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de actualizar la información el Genero";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de actualizar el Genero";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			factoria.cerrarConexion();
		}
	}

	@Override
	public void eliminar(GeneroDTO genero) {
		
		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		
		try {
			factoria.iniciarTransaccion();
			
			final IGeneroNegocio negocio = new GeneroNegocio(factoria);
			negocio.eliminar(genero);
			
			factoria.confirmarTransaccion();
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de eliminar la información del Genero";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de actualizar el Genero";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			factoria.cerrarConexion();
		}
	}

	@Override
	public List<GeneroDTO> consultar(GeneroDTO genero) {
		
		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		
		List<GeneroDTO> generos;
		
		try {
			factoria.iniciarTransaccion();
			
			final IGeneroNegocio negocio = new GeneroNegocio(factoria);
			generos = negocio.consultar(genero);
			
			factoria.confirmarTransaccion();
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de consultar la información del Genero";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de consultar el Genero";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			factoria.cerrarConexion();
		}
		
		return generos;
	}
	
}
