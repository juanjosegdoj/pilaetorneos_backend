package com.pilae.pilaetorneo.negocio.fachada.concreta;

import java.util.List;

import com.pilae.pilaeTorneo.datos.factoria.abstracta.FactoriaDAO;
import com.pilae.pilaetorneo.dto.CategoriaDTO;
import com.pilae.pilaetorneo.negocio.fachada.ICategoriaFachada;
import com.pilae.pilaetorneo.negocio.negocio.ICategoriaNegocio;
import com.pilae.pilaetorneo.negocio.negocio.ICategoriaNegocio;
import com.pilae.pilaetorneo.negocio.negocio.ICategoriaNegocio;
import com.pilae.pilaetorneo.negocio.negocio.concreta.CategoriaNegocio;
import com.pilae.pilaetorneo.negocio.negocio.concreta.CategoriaNegocio;
import com.pilae.pilaetorneo.negocio.negocio.concreta.CategoriaNegocio;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import com.pilae.pilaetorneo.utilitarios.sql.enumeracion.FuenteInformacionEnum;

public class CategoriaFachada implements ICategoriaFachada {
	
	@Override
	public void crear(CategoriaDTO categoria) {

		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		
		try {
			factoria.iniciarTransaccion();
			
			final ICategoriaNegocio negocio = new CategoriaNegocio(factoria);
			negocio.crear(categoria);
			
			factoria.confirmarTransaccion();
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de registrar la información del nuevo Categoria";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de crear el nuevo Categoria";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			factoria.cerrarConexion();
		}
		
	}

	@Override
	public void actualizar(CategoriaDTO categoria) {
		
		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		
		try {
			factoria.iniciarTransaccion();
			
			final ICategoriaNegocio negocio = new CategoriaNegocio(factoria);
			negocio.actualizar(categoria);
			
			factoria.confirmarTransaccion();
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de actualizar la información de la Categoria";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de actualizar la información de la Categoria";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			factoria.cerrarConexion();
		}
	}

	@Override
	public void eliminar(CategoriaDTO categoria) {
		
		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		
		try {
			factoria.iniciarTransaccion();
			
			final ICategoriaNegocio negocio = new CategoriaNegocio(factoria);
			negocio.eliminar(categoria);
			
			factoria.confirmarTransaccion();
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de eliminar la información de la Categoria";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de eliminar la Categoria.";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			factoria.cerrarConexion();
		}
		
	}

	@Override
	public List<CategoriaDTO> consultar(CategoriaDTO categoria) {
		
		final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria(FuenteInformacionEnum.MY_SQL);
		
		List<CategoriaDTO> categorias;
		try {
			
			factoria.iniciarTransaccion();
			final ICategoriaNegocio negocio = new CategoriaNegocio(factoria);
			
			categorias = negocio.consultar(categoria);
			
			factoria.confirmarTransaccion();
			
		} catch (final AplicacionExcepcion excepcion) {
			factoria.cancelarTransaccion();
			
			throw excepcion;
		} catch (final Exception excepcion) {
			factoria.cancelarTransaccion();
			
			final String mensajeUsuario = "Se ha presentado un problema tratando de consultar la información de las categorias.";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de consultar las categorias.";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
		} finally {
			
			factoria.cerrarConexion();
		}
		return categorias;
	}
	
}
