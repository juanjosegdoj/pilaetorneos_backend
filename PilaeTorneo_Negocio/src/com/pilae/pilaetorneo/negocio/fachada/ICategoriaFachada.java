package com.pilae.pilaetorneo.negocio.fachada;

import java.util.List;

import com.pilae.pilaetorneo.dto.CategoriaDTO;

public interface ICategoriaFachada {
	
	void crear(CategoriaDTO categoria);
	
	void actualizar(CategoriaDTO categoria);
	
	void eliminar(CategoriaDTO categoria);
	
	List<CategoriaDTO> consultar(CategoriaDTO categoria);
}
