package com.pilae.pilaetorneo.negocio.fachada;

import java.util.List;

import com.pilae.pilaetorneo.dto.EstadoDTO;

public interface IEstadoFachada {
	
	void crear(EstadoDTO estado);
	
	void actualizar(EstadoDTO estado);
	
	void eliminar(EstadoDTO estado);
	
	List<EstadoDTO> consultar(EstadoDTO deporte);
}
