package com.pilae.pilaetorneo.negocio.fachada;

import java.util.List;

import com.pilae.pilaetorneo.dto.DeporteDTO;

public interface IDeporteFachada {
	
	void crear(DeporteDTO deporte);
	
	void actualizar(DeporteDTO deporte);
	
	void eliminar(DeporteDTO deporte);
	
	List<DeporteDTO> consultar(DeporteDTO deporte);
}
