package com.pilae.pilaetorneo.negocio.fachada;

import java.util.List;

import com.pilae.pilaetorneo.dto.PatrocinadorDTO;

public interface IPatrocinadorFachada {
	
	void crear(PatrocinadorDTO patrocinador);
	
	void actualizar(PatrocinadorDTO patrocinador);
	
	void eliminar(PatrocinadorDTO patrocinador);
	
	List<PatrocinadorDTO> consultar(PatrocinadorDTO patrocinador);
}
