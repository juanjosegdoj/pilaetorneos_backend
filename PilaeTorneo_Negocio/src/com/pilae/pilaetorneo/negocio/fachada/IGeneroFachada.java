package com.pilae.pilaetorneo.negocio.fachada;

import java.util.List;

import com.pilae.pilaetorneo.dto.GeneroDTO;

public interface IGeneroFachada {
	
	void crear(GeneroDTO genero);
	
	void actualizar(GeneroDTO genero);
	
	void eliminar(GeneroDTO genero);
	
	List<GeneroDTO> consultar(GeneroDTO genero);

}
