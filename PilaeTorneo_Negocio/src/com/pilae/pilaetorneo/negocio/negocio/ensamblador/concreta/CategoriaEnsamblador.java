package com.pilae.pilaetorneo.negocio.negocio.ensamblador.concreta;

import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;

import java.util.ArrayList;
import java.util.List;

import com.pilae.pilaetorneo.dominio.CategoriaDominio;
import com.pilae.pilaetorneo.dominio.DeporteDominio;
import com.pilae.pilaetorneo.dto.CategoriaDTO;
import com.pilae.pilaetorneo.dto.DeporteDTO;
import com.pilae.pilaetorneo.negocio.negocio.ensamblador.IEnsamblador;
import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;

public class CategoriaEnsamblador implements IEnsamblador<CategoriaDominio, CategoriaDTO> {
	
	private static final IEnsamblador<CategoriaDominio, CategoriaDTO> INSTANCIA = new CategoriaEnsamblador();
	
	private CategoriaEnsamblador() {
		super();
	}
	
	public static IEnsamblador<CategoriaDominio, CategoriaDTO> obtenercategoriaEnsamblador(){
		return INSTANCIA;
	}

	@Override
	public final CategoriaDTO ensamblarDTO(CategoriaDominio objetoDominio) {
		if(obtenerUtilObjeto().objetoEsNulo(objetoDominio)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando realizar la operaci�n deseada sobre una Categoria";
			final String mensajeTecnico = "No es posible ensamblar una CategoriaDTO con una CategoriaDominio nula.";

			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		DeporteDTO deporteDTO = DeporteEnsamblador.obtenerDeporteEnsamblador().ensamblarDTO(objetoDominio.getDeporte());
		return new CategoriaDTO(objetoDominio.getIdCategoria(), objetoDominio.getNombre(), objetoDominio.getFechaNacimientoMaxima(), objetoDominio.getFechaNacimientoMinimo(), deporteDTO, objetoDominio.getAnio());
		
	}

	@Override
	public final CategoriaDominio ensamblarDominio(CategoriaDTO objetoDto, OperacionEnum operacion) {
		
		
		if(obtenerUtilObjeto().objetoEsNulo(operacion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando realizar la operaci�n deseada sobre una Categoria.";
			final String mensajeTecnico = "No es posible ensamblar una CategoriaDominio con la operaci�n vac�a. La operaci�n es necesaria para asegurar la integridad";

			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		// mapeo los objetos de los cuales depende directamente Categoria, en este caso Deporte
		final DeporteDTO mapDeporteDTO = obtenerUtilObjeto().obtenerValorDefecto(objetoDto.getDeporte(), new DeporteDTO());	
		final DeporteDominio mapDeporteDominio = DeporteEnsamblador.obtenerDeporteEnsamblador().ensamblarDominio(mapDeporteDTO, OperacionEnum.CONSULTAR);
		
		// Despu�s mapeo mi objeto en cuesti�n		
		final CategoriaDTO mapeadorDTO = obtenerUtilObjeto().obtenerValorDefecto(objetoDto, new CategoriaDTO());
				
		return  CategoriaDominio.crear(mapeadorDTO.getIdCategoria(), mapeadorDTO.getNombre(), mapeadorDTO.getFechaNacimientoMaxima(), mapeadorDTO.getFechaNacimientoMinimo(), mapDeporteDominio, mapeadorDTO.getAnio(), operacion); 
		
		
	}

	@Override
	public List<CategoriaDTO> enmsablarListaDTO(List<CategoriaDominio> listaObjetosDominio) {
		
		final List<CategoriaDominio> listaMapeadorDominio = obtenerUtilObjeto().obtenerValorDefecto(listaObjetosDominio, new ArrayList<>());
		final List<CategoriaDTO> listaDtos = new ArrayList<>();
		
		for(final CategoriaDominio categoriaDominio: listaMapeadorDominio) {
			listaDtos.add(ensamblarDTO(categoriaDominio));
		}
		
		return listaDtos;
	}

	
}
