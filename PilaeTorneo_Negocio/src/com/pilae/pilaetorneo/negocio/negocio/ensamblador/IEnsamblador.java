package com.pilae.pilaetorneo.negocio.negocio.ensamblador;

import java.util.List;

import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;

public interface IEnsamblador<D, T> {
	
	T ensamblarDTO(D objetoDominio);

	D ensamblarDominio(T objetoDto, OperacionEnum operacion);

	List<T> enmsablarListaDTO(List<D> listaObjetosDominio);
	
}
