package com.pilae.pilaetorneo.negocio.negocio.ensamblador.concreta;

import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;

import java.util.ArrayList;
import java.util.List;

import com.pilae.pilaetorneo.dominio.GeneroDominio;
import com.pilae.pilaetorneo.dto.GeneroDTO;
import com.pilae.pilaetorneo.negocio.negocio.ensamblador.IEnsamblador;
import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;

public final class GeneroEnsamblador implements IEnsamblador<GeneroDominio, GeneroDTO>{

	private static IEnsamblador<GeneroDominio, GeneroDTO> INSTANCIA = new GeneroEnsamblador();
	
	private GeneroEnsamblador() {
		super();
	}
	
	public static IEnsamblador<GeneroDominio, GeneroDTO> obtenerGeneroEnsamblador(){
		return INSTANCIA;
	}
	
	@Override
	public final GeneroDTO ensamblarDTO(GeneroDominio objetoDominio) {
		if(obtenerUtilObjeto().objetoEsNulo(objetoDominio)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando realizar la operaci�n deseada sobre un Genero";
			final String mensajeTecnico = "No es posible ensamblar un GeneroDTO con un GeneroDominio nulo.";

			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		return new GeneroDTO(objetoDominio.getIdGenero(), objetoDominio.getNombre());
	}

	@Override
	public final GeneroDominio ensamblarDominio(GeneroDTO objetoDto, OperacionEnum operacion) {
		
		if(obtenerUtilObjeto().objetoEsNulo(operacion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando realizar la operaci�n deseada sobre un Genero";
			final String mensajeTecnico = "No es posible ensamblar un GeneroDominio con la operaci�n vac�a. La operaci�n es necesaria para asegurar la integridad";

			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		final GeneroDTO mapeadorDTO = obtenerUtilObjeto().obtenerValorDefecto(objetoDto, new GeneroDTO());
		
		return GeneroDominio.crear(mapeadorDTO.getIdGenero(), mapeadorDTO.getNombre(), operacion);
	}

	@Override
	public final List<GeneroDTO> enmsablarListaDTO(List<GeneroDominio> listaObjetosDominio) {
		
		final List<GeneroDominio> listaMapeadorDominio = obtenerUtilObjeto().obtenerValorDefecto(listaObjetosDominio, new ArrayList<>());
		final List<GeneroDTO> listaDtos = new ArrayList<>();
		
		for(final GeneroDominio generoDominio : listaMapeadorDominio) {
			listaDtos.add(ensamblarDTO(generoDominio));
		}
		
		return listaDtos;
	}

}
