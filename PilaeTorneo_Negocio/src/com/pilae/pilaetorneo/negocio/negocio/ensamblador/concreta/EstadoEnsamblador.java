package com.pilae.pilaetorneo.negocio.negocio.ensamblador.concreta;

import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;

import java.util.ArrayList;
import java.util.List;

import com.pilae.pilaetorneo.dominio.EstadoDominio;
import com.pilae.pilaetorneo.dto.EstadoDTO;
import com.pilae.pilaetorneo.negocio.negocio.ensamblador.IEnsamblador;
import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;

public class EstadoEnsamblador implements IEnsamblador<EstadoDominio, EstadoDTO> {
	
	private static final IEnsamblador<EstadoDominio, EstadoDTO> INSTANCIA = new EstadoEnsamblador();
	
	public EstadoEnsamblador() {
		super();
	}
	
	public static IEnsamblador<EstadoDominio, EstadoDTO> obtenerEstadoEnsamblador(){
		return INSTANCIA;
	}
	
	@Override
	public EstadoDTO ensamblarDTO(EstadoDominio objetoDominio) {
		if(obtenerUtilObjeto().objetoEsNulo(objetoDominio)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando realizar la operaci�n deseada sobre un Estado";
			final String mensajeTecnico = "No es posible ensamblar un EstadoDTO con un EstadoDominio nulo.";

			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		return new EstadoDTO(objetoDominio.getIdEstado(), objetoDominio.getNombre(), objetoDominio.getDescripcion());
	}

	@Override
	public EstadoDominio ensamblarDominio(EstadoDTO objetoDto, OperacionEnum operacion) {
		
		if(obtenerUtilObjeto().objetoEsNulo(operacion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando realizar la operaci�n deseada sobre un Estado";
			final String mensajeTecnico = "No es posible ensamblar un EstadoDominio con la operaci�n vac�a. La operaci�n es necesaria para asegurar la integridad";

			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		final EstadoDTO mapeadorDTO = obtenerUtilObjeto().obtenerValorDefecto(objetoDto, new EstadoDTO());		
		
		return EstadoDominio.crear(mapeadorDTO.getIdEstado(), mapeadorDTO.getNombre(), mapeadorDTO.getDescripcion(), operacion);
	}

	@Override
	public List<EstadoDTO> enmsablarListaDTO(List<EstadoDominio> listaObjetosDominio) {
		final List<EstadoDominio> listaMapeadorDominio = obtenerUtilObjeto().obtenerValorDefecto(listaObjetosDominio, new ArrayList<>());
		final List<EstadoDTO> listaDtos = new ArrayList<>();
		
		for(final EstadoDominio estadoDominio : listaMapeadorDominio) {
			listaDtos.add(ensamblarDTO(estadoDominio));
		}
		
		return listaDtos;
	}

}
