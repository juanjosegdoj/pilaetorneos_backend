package com.pilae.pilaetorneo.negocio.negocio.ensamblador.concreta;

import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;

import java.util.ArrayList;
import java.util.List;

import com.pilae.pilaetorneo.dominio.PatrocinadorDominio;
import com.pilae.pilaetorneo.dto.PatrocinadorDTO;
import com.pilae.pilaetorneo.negocio.negocio.ensamblador.IEnsamblador;
import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;

public class PatrocinadorEnsamblador implements IEnsamblador<PatrocinadorDominio, PatrocinadorDTO>{
	
	private static final IEnsamblador<PatrocinadorDominio, PatrocinadorDTO> INSTANCIA = new PatrocinadorEnsamblador();
	
	private PatrocinadorEnsamblador() {
		super();
	}
	
	public static IEnsamblador<PatrocinadorDominio, PatrocinadorDTO> obtenerPatrocinadorEnsamblador(){
		return INSTANCIA;
	}
	
	@Override
	public final PatrocinadorDTO ensamblarDTO(final PatrocinadorDominio objetoDominio) {
		if(obtenerUtilObjeto().objetoEsNulo(objetoDominio)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando realizar la operaci�n deseada sobre un patrocinador";
			final String mensajeTecnico = "No es posible ensamblar un PatrocinadorDTO con un PatrocinadorDominio nulo.";

			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		return new PatrocinadorDTO(objetoDominio.getIdPatrocinador(), objetoDominio.getNombre(), objetoDominio.getDescripcion());
	}

	@Override
	public final PatrocinadorDominio ensamblarDominio(final PatrocinadorDTO objetoDto, final OperacionEnum operacion) {
		
		if(obtenerUtilObjeto().objetoEsNulo(operacion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando realizar la operaci�n deseada sobre un patrocinador";
			final String mensajeTecnico = "No es posible ensamblar un PatrocinadorDominio con la operaci�n vac�a. La operaci�n es necesaria para asegurar la integridad";

			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		final PatrocinadorDTO mapeadorDTO = obtenerUtilObjeto().obtenerValorDefecto(objetoDto, new PatrocinadorDTO());		
		
		return PatrocinadorDominio.crear(mapeadorDTO.getIdPatrocinador(), mapeadorDTO.getNombre(), mapeadorDTO.getDescripcion(), operacion);
	}

	@Override
	public final List<PatrocinadorDTO> enmsablarListaDTO(List<PatrocinadorDominio> listaObjetosDominio) {
		
		final List<PatrocinadorDominio> listaMapeadorDominio = obtenerUtilObjeto().obtenerValorDefecto(listaObjetosDominio, new ArrayList<>());
		final List<PatrocinadorDTO> listaDtos = new ArrayList<>();
		
		for(final PatrocinadorDominio PatrocinadorDominio : listaMapeadorDominio) {
			listaDtos.add(ensamblarDTO(PatrocinadorDominio));
		}
		
		return listaDtos;
	}
}
