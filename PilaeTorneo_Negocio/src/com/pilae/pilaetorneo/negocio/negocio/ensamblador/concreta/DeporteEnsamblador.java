package com.pilae.pilaetorneo.negocio.negocio.ensamblador.concreta;

import java.util.ArrayList;
import java.util.List;

import com.pilae.pilaetorneo.dominio.DeporteDominio;
import com.pilae.pilaetorneo.dto.DeporteDTO;
import com.pilae.pilaetorneo.negocio.negocio.ensamblador.IEnsamblador;
import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;

import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;;

public final class DeporteEnsamblador implements IEnsamblador<DeporteDominio, DeporteDTO> {
	
	private static final IEnsamblador<DeporteDominio, DeporteDTO> INSTANCIA = new DeporteEnsamblador();
	
	private DeporteEnsamblador() {
		super();
	}
	
	public static IEnsamblador<DeporteDominio, DeporteDTO> obtenerDeporteEnsamblador(){
		return INSTANCIA;
	}
	
	@Override
	public final DeporteDTO ensamblarDTO(final DeporteDominio objetoDominio) {
		if(obtenerUtilObjeto().objetoEsNulo(objetoDominio)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando realizar la operaci�n deseada sobre un Deporte";
			final String mensajeTecnico = "No es posible ensamblar un DeporteDTO con un DeporteDominio nulo.";

			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		return new DeporteDTO(objetoDominio.getIdDeporte(), objetoDominio.getNombre(), objetoDominio.getDescripcion());
	}

	@Override
	public final DeporteDominio ensamblarDominio(final DeporteDTO objetoDto, final OperacionEnum operacion) {
		
		if(obtenerUtilObjeto().objetoEsNulo(operacion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando realizar la operaci�n deseada sobre un Deporte";
			final String mensajeTecnico = "No es posible ensamblar un DeporteDominio con la operaci�n vac�a. La operaci�n es necesaria para asegurar la integridad";

			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		final DeporteDTO mapeadorDTO = obtenerUtilObjeto().obtenerValorDefecto(objetoDto, new DeporteDTO());		
		
		return DeporteDominio.crear(mapeadorDTO.getIdDeporte(), mapeadorDTO.getNombre(), mapeadorDTO.getDescripcion(), operacion);
	}

	@Override
	public final List<DeporteDTO> enmsablarListaDTO(List<DeporteDominio> listaObjetosDominio) {
		
		final List<DeporteDominio> listaMapeadorDominio = obtenerUtilObjeto().obtenerValorDefecto(listaObjetosDominio, new ArrayList<>());
		final List<DeporteDTO> listaDtos = new ArrayList<>();
		
		for(final DeporteDominio deporteDominio : listaMapeadorDominio) {
			listaDtos.add(ensamblarDTO(deporteDominio));
		}
		
		return listaDtos;
	}

}
