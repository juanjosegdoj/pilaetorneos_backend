package com.pilae.pilaetorneo.negocio.negocio;

import java.util.List;

import com.pilae.pilaetorneo.dto.PatrocinadorDTO;

public interface IPatrocinadorNegocio {
	void crear(PatrocinadorDTO patrocinador);
	
	void actualizar(PatrocinadorDTO patrocinador);
	
	void eliminar(PatrocinadorDTO patrocinador);
	
	List<PatrocinadorDTO> consultar(PatrocinadorDTO patrocinador);
}
