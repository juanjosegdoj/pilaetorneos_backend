package com.pilae.pilaetorneo.negocio.negocio;

import java.util.List;

import com.pilae.pilaetorneo.dto.GeneroDTO;

public interface IGeneroNegocio {
	
	void crear(GeneroDTO genero);
	
	void actualizar(GeneroDTO genero);
	
	void eliminar(GeneroDTO genero);
	
	List<GeneroDTO> consultar(GeneroDTO genero);
}
