package com.pilae.pilaetorneo.negocio.negocio;

import java.util.List;

import com.pilae.pilaetorneo.dto.EstadoDTO;

public interface IEstadoNegocio {
	
	void crear(EstadoDTO estado);
	
	void actualizar(EstadoDTO estado);
	
	void eliminar(EstadoDTO estado);
	
	List<EstadoDTO> consultar(EstadoDTO deporte);
}
