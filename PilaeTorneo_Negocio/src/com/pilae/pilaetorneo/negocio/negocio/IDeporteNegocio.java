package com.pilae.pilaetorneo.negocio.negocio;

import java.util.List;

import com.pilae.pilaetorneo.dto.DeporteDTO;

public interface IDeporteNegocio {
	
	void crear(DeporteDTO deporte);
	
	void actualizar(DeporteDTO deporte);
	
	void eliminar(DeporteDTO deporte);
	
	List<DeporteDTO> consultar(DeporteDTO deporte);
}
