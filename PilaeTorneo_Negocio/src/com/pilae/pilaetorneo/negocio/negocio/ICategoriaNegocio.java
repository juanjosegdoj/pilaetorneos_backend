package com.pilae.pilaetorneo.negocio.negocio;

import java.util.List;

import com.pilae.pilaetorneo.dto.CategoriaDTO;

public interface ICategoriaNegocio {
	
	void crear(CategoriaDTO categoria);
	
	void actualizar(CategoriaDTO categoria);
	
	void eliminar(CategoriaDTO categoria);
	
	List<CategoriaDTO> consultar(CategoriaDTO categoria);
}
