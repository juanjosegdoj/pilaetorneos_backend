package com.pilae.pilaetorneo.negocio.negocio.concreta;

import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;

import java.util.List;

import com.pilae.pilaeTorneo.datos.factoria.abstracta.FactoriaDAO;
import com.pilae.pilaetorneo.dominio.EstadoDominio;
import com.pilae.pilaetorneo.dto.EstadoDTO;
import com.pilae.pilaetorneo.negocio.negocio.IEstadoNegocio;
import com.pilae.pilaetorneo.negocio.negocio.ensamblador.concreta.EstadoEnsamblador;
import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;

public class EstadoNegocio implements IEstadoNegocio{

	private final FactoriaDAO factoria;
	
	public EstadoNegocio(final FactoriaDAO factoria) {
		super();
		
		if(obtenerUtilObjeto().objetoEsNulo(factoria)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando realizar la operaci�n deseada sobre un Estado";
			final String mensajeTecnico = "No es posible crear un EstadoNegocio con una factoria nula.";

			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		this.factoria = factoria;
	}
	
	public final void crear(EstadoDTO estado) {
		
		// 1. Ensamblar dominio
		final EstadoDominio estadoDominio = EstadoEnsamblador.obtenerEstadoEnsamblador().ensamblarDominio(estado, OperacionEnum.CREAR);
		
		// 2. Regla Uno: Validar que no exista un Estado con el nombre deseado
		final EstadoDominio validarReglaUno = EstadoDominio.crear(0, estadoDominio.getNombre(), "", OperacionEnum.CONSULTAR);
		final List<EstadoDominio> registrosReglaUno = factoria.obtenerEstadoDAO().consultar(validarReglaUno);
		if(!registrosReglaUno.isEmpty()) {
			final String mensajeUsuario = "Ya existe un estado con el nombre ingresado";
			final String mensajeTecnico = "No se pueden registar dos Estados con el mismo nombre";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		// 3. Crear el Estado en la fuente de informaci�n
		factoria.obtenerEstadoDAO().crear(estadoDominio);
	}

	@Override
	public final void actualizar(EstadoDTO estado) {
		
		// 1. Ensamblar dominio
		final EstadoDominio estadoDominio = EstadoEnsamblador.obtenerEstadoEnsamblador().ensamblarDominio(estado, OperacionEnum.ACTUALIZAR);
		
		// 2. Regla Uno: Validar que exista un Estado con el nombre deseado
		final EstadoDominio validarReglaUno = EstadoDominio.crear(estadoDominio.getIdEstado(), "", "", OperacionEnum.CONSULTAR);
		final List<EstadoDominio> registrosReglaUno = factoria.obtenerEstadoDAO().consultar(validarReglaUno);
	
		
		if(registrosReglaUno.isEmpty()) {
			final String mensajeUsuario = "No es posible actualizar un estado no existente";
			final String mensajeTecnico = "No se puede actualizar el Estado, ya que el id_estado no existe";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		// 3. Regla Dos: No exista otro Estado con el nombre ingresado.
		final EstadoDominio validarReglaDos = EstadoDominio.crear(0, estado.getNombre(), "", OperacionEnum.CONSULTAR);
		final List<EstadoDominio> registrosReglaDos = factoria.obtenerEstadoDAO().consultar(validarReglaDos);
		
		if(!registrosReglaDos.isEmpty() && registrosReglaDos.get(0).getIdEstado() != estado.getIdEstado()) {
			final String mensajeUsuario = "Ya existe un Estado con el nombre ingresado.";
			final String mensajeTecnico = "No se pueden resgistrar 2 estados con el mismo nombre.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		
		// 4. Lo actualizo si cambiaron los datos
		if(!registrosReglaUno.get(0).getNombre().equals(estadoDominio.getNombre())
				||  !registrosReglaUno.get(0).getDescripcion().equals(estadoDominio.getDescripcion())) {
			
			factoria.obtenerEstadoDAO().actualizar(estadoDominio);
		}		
	}

	@Override
	public final void eliminar(EstadoDTO estado) {
		
		// 1. Ensamblar dominio
		final EstadoDominio estadoDominio = EstadoEnsamblador.obtenerEstadoEnsamblador().ensamblarDominio(estado, OperacionEnum.ELIMINAR);
		
		// 2. Regla Uno: Validar que exista un Estado con el nombre deseado
		final EstadoDominio validarReglaUno = EstadoDominio.crear(estado.getIdEstado(), "", "", OperacionEnum.CONSULTAR);
		final List<EstadoDominio> registrosReglaUno = factoria.obtenerEstadoDAO().consultar(validarReglaUno);
		if(registrosReglaUno.isEmpty()) {
			final String mensajeUsuario = "No es posible eliminar un estado no existente";
			final String mensajeTecnico = "No se puede eliminar el Estado, ya que el id_estado no existe";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		// 3. Validar que esa entidad no se est� usando en ning�n otro lugar como llave for�nea
		
		// 4. Eliminar el estado de la fuente de informaci�n
		factoria.obtenerEstadoDAO().eliminar(estadoDominio);
	}

	@Override
	public final List<EstadoDTO> consultar(EstadoDTO estado) {
		
		// 1. Ensamblar dominio
		final EstadoDominio estadoDominio = EstadoEnsamblador.obtenerEstadoEnsamblador().ensamblarDominio(estado, OperacionEnum.CONSULTAR);
		
		// 2. Crear el Estado en la fuente de informaci�n
		final List<EstadoDominio> listaDominios = factoria.obtenerEstadoDAO().consultar(estadoDominio);

		return EstadoEnsamblador.obtenerEstadoEnsamblador().enmsablarListaDTO(listaDominios);
	}
}
