package com.pilae.pilaetorneo.negocio.negocio.concreta;

import java.util.List;

import com.pilae.pilaeTorneo.datos.factoria.abstracta.FactoriaDAO;
import com.pilae.pilaetorneo.dominio.DeporteDominio;
import com.pilae.pilaetorneo.dto.DeporteDTO;
import com.pilae.pilaetorneo.negocio.negocio.IDeporteNegocio;
import com.pilae.pilaetorneo.negocio.negocio.ensamblador.concreta.DeporteEnsamblador;
import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;

import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;


public class DeporteNegocio implements IDeporteNegocio{
	
	private final FactoriaDAO factoria;
	
	public DeporteNegocio(final FactoriaDAO factoria) {
		super();
		
		if(obtenerUtilObjeto().objetoEsNulo(factoria)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando realizar la operacin deseada sobre un Deporte";
			final String mensajeTecnico = "No es posible crear un DeporteNegocio con una factoria nula.";

			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		this.factoria = factoria;
	}
	
	public final void crear(DeporteDTO deporte) {
		
		// 1. Ensamblar dominio
		final DeporteDominio deporteDominio = DeporteEnsamblador.obtenerDeporteEnsamblador().ensamblarDominio(deporte, OperacionEnum.CREAR);
		
		// 2. Regla Uno: Validar que no exista un Deporte con el nombre deseado
		final DeporteDominio validarReglaUno = DeporteDominio.crear(0, deporteDominio.getNombre(), "", OperacionEnum.CONSULTAR);
		final List<DeporteDominio> registrosReglaUno = factoria.obtenerDeporteDAO().consultar(validarReglaUno);
		if(!registrosReglaUno.isEmpty()) {
			final String mensajeUsuario = "Ya existe un deporte con el nombre ingresado";
			final String mensajeTecnico = "No se pueden registar dos Deportes con el mismo nombre";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		// 3. Crear el Deporte en la fuente de informaci�n
		factoria.obtenerDeporteDAO().crear(deporteDominio);
	}

	@Override
	public final void actualizar(DeporteDTO deporte) {
		
		// 1. Ensamblar dominio
		final DeporteDominio deporteDominio = DeporteEnsamblador.obtenerDeporteEnsamblador().ensamblarDominio(deporte, OperacionEnum.ACTUALIZAR);
		
		// 2. Regla Uno: Validar que exista un Deporte con el nombre deseado
		final DeporteDominio validarReglaUno = DeporteDominio.crear(deporte.getIdDeporte(), "", "", OperacionEnum.CONSULTAR);
		final List<DeporteDominio> registrosReglaUno = factoria.obtenerDeporteDAO().consultar(validarReglaUno);
		
		if(registrosReglaUno.isEmpty()) {
			final String mensajeUsuario = "No es posible actualizar un deporte no existente";
			final String mensajeTecnico = "No se puede actualizar el Deporte, ya que el id_deporte no existe";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		// 3. Regla Dos: No exista otro Deporte con el nombre ingresado.
		final DeporteDominio validarReglaDos = DeporteDominio.crear(0, deporte.getNombre(), "", OperacionEnum.CONSULTAR);
		final List<DeporteDominio> registrosReglaDos = factoria.obtenerDeporteDAO().consultar(validarReglaDos);
		
		if(!registrosReglaDos.isEmpty() && registrosReglaDos.get(0).getIdDeporte() != deporte.getIdDeporte()) {
			final String mensajeUsuario = "Ya existe un Deporte con el nombre ingresado";
			final String mensajeTecnico = "No se pueden resgistrar 2 deportes con el mismo nombre";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		
		// 4. Lo actualizo si cambiaron los datos
		if(!registrosReglaUno.get(0).getNombre().equals(deporteDominio.getNombre())
				||  !registrosReglaUno.get(0).getDescripcion().equals(deporteDominio.getDescripcion())) {
			
			factoria.obtenerDeporteDAO().actualizar(deporteDominio);
		}		
	}

	@Override
	public final void eliminar(DeporteDTO deporte) {
		
		// 1. Ensamblar dominio
		final DeporteDominio deporteDominio = DeporteEnsamblador.obtenerDeporteEnsamblador().ensamblarDominio(deporte, OperacionEnum.ELIMINAR);
		
		// 2. Regla Uno: Validar que exista un Deporte con el id_deporte deseado
		final DeporteDominio validarReglaUno = DeporteDominio.crear(deporte.getIdDeporte(), "", "", OperacionEnum.CONSULTAR);
		final List<DeporteDominio> registrosReglaUno = factoria.obtenerDeporteDAO().consultar(validarReglaUno);
		if(registrosReglaUno.isEmpty()) {
			final String mensajeUsuario = "No es posible eliminar un deporte no existente";
			final String mensajeTecnico = "No se puede eliminar el Deporte, ya que el id_deporte no existe";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		// 3. Validar que esa entidad no se est� usando en ning�n otro lugar como llave for�nea
		
		// 4. Eliminar el deporte de la fuente de informaci�n
		factoria.obtenerDeporteDAO().eliminar(deporteDominio);
	}

	@Override
	public final List<DeporteDTO> consultar(DeporteDTO deporte) {
		
		// 1. Ensamblar dominio
		final DeporteDominio deporteDominio = DeporteEnsamblador.obtenerDeporteEnsamblador().ensamblarDominio(deporte, OperacionEnum.CONSULTAR);
		
		// 2. Crear el tipo de cuenta en la fuente de informaci�n
		final List<DeporteDominio> listaDominios = factoria.obtenerDeporteDAO().consultar(deporteDominio);
		return DeporteEnsamblador.obtenerDeporteEnsamblador().enmsablarListaDTO(listaDominios);
	}
	
}
