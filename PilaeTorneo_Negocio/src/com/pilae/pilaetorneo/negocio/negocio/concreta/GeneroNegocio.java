package com.pilae.pilaetorneo.negocio.negocio.concreta;

import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;

import java.util.List;

import com.pilae.pilaeTorneo.datos.factoria.abstracta.FactoriaDAO;
import com.pilae.pilaetorneo.dominio.GeneroDominio;
import com.pilae.pilaetorneo.dto.GeneroDTO;
import com.pilae.pilaetorneo.negocio.negocio.IGeneroNegocio;
import com.pilae.pilaetorneo.negocio.negocio.ensamblador.concreta.GeneroEnsamblador;
import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;

public class GeneroNegocio implements IGeneroNegocio{
	
	private final FactoriaDAO factoria;
	
	
	public GeneroNegocio(FactoriaDAO factoria) {
		super();
		
		if(obtenerUtilObjeto().objetoEsNulo(factoria)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando realizar la operaci�n deseada sobre un G�nero";
			final String mensajeTecnico = "No es posible crear un GeneroNegocio con una factoria nula.";

			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		this.factoria = factoria;
	}

	@Override
	public void crear(GeneroDTO genero) {
		// 1. Ensamblar Dominio
		final GeneroDominio generoDominio = GeneroEnsamblador.obtenerGeneroEnsamblador().ensamblarDominio(genero, OperacionEnum.CREAR);
		
		// 2. Reglar Uno: Validar que no exista un G�nero con el nombre
		final GeneroDominio validarReglaUno = GeneroDominio.crear(0, genero.getNombre(), OperacionEnum.CONSULTAR);
		final List<GeneroDominio> registrosReglaUno = factoria.obtenerGeneroDAO().consultar(validarReglaUno);
		if(!registrosReglaUno.isEmpty()) {
			final String mensajeUsuario = "Ya existe un genero con el nombre ingresado";
			final String mensajeTecnico = "No se pueden registar dos Generos con el mismo nombre";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		//3. Crear el genero en la fuente de informaci�n
		factoria.obtenerGeneroDAO().crear(generoDominio);
	}

	@Override
	public void actualizar(GeneroDTO genero) {
		
		// 1. Ensamblar Dominio
		final GeneroDominio generoDominio = GeneroEnsamblador.obtenerGeneroEnsamblador().ensamblarDominio(genero, OperacionEnum.ACTUALIZAR);
		
		// 2. Reglar Uno: Validar que exista el G�nero con ese ID
		final GeneroDominio validarReglaUno = GeneroDominio.crear(genero.getIdGenero(), "", OperacionEnum.CONSULTAR);
		final List<GeneroDominio> registrosReglaUno = factoria.obtenerGeneroDAO().consultar(validarReglaUno);
		
		if(registrosReglaUno.isEmpty()) {
			final String mensajeUsuario = "No es posible actualizar un Genero no existente.";
			final String mensajeTecnico = "No se puede actualizar el Genero, ya que el id_genero no existe";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}

		// 3. Regla Dos: No exista otro Genero con el nombre ingresado.
		final GeneroDominio validarReglaDos = GeneroDominio.crear(0, genero.getNombre(), OperacionEnum.CONSULTAR);
		final List<GeneroDominio> registrosReglaDos = factoria.obtenerGeneroDAO().consultar(validarReglaDos);
				
		if(!registrosReglaDos.isEmpty() && registrosReglaDos.get(0).getIdGenero() != genero.getIdGenero()) {
			final String mensajeUsuario = "Ya existe un Genero con el nombre ingresado";
			final String mensajeTecnico = "No se pueden resgistrar 2 generos con el mismo nombre";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}

		//4. Crear el genero en la fuente de informaci�n
		factoria.obtenerGeneroDAO().actualizar(generoDominio);
		
	}

	@Override
	public void eliminar(GeneroDTO genero) {
		
		// 1. Ensamblar Dominio
		final GeneroDominio generoDominio = GeneroEnsamblador.obtenerGeneroEnsamblador().ensamblarDominio(genero, OperacionEnum.ELIMINAR);
		
		// 2. Reglar Uno: Validar que exista un G�nero con el ID deseado
		final GeneroDominio validarReglaUno = GeneroDominio.crear(genero.getIdGenero(), "", OperacionEnum.CONSULTAR);
		final List<GeneroDominio> registrosReglaUno = factoria.obtenerGeneroDAO().consultar(validarReglaUno);
		if(registrosReglaUno.isEmpty()) {
			final String mensajeUsuario = "No es posible eliminar un genero no existente";
			final String mensajeTecnico = "No se puede eliminar el genero, ya que el id_genero no existe";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		//3. Validar que esa entidad no se est� usando en ning�n otro lugar como llave for�nea
		
		//4. Crear el genero en la fuente de informaci�n
		factoria.obtenerGeneroDAO().eliminar(generoDominio);
		
	}

	@Override
	public List<GeneroDTO> consultar(GeneroDTO genero) {
		
		// 1. Ensamblar Dominio
		final GeneroDominio generoDominio = GeneroEnsamblador.obtenerGeneroEnsamblador().ensamblarDominio(genero, OperacionEnum.CONSULTAR);
		
		// 2. Crear el g�nero en la fuente de informaci�n
		final List<GeneroDominio> listaDominios = factoria.obtenerGeneroDAO().consultar(generoDominio);
		return GeneroEnsamblador.obtenerGeneroEnsamblador().enmsablarListaDTO(listaDominios);
	}

}
