package com.pilae.pilaetorneo.negocio.negocio.concreta;

import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;

import java.util.List;

import com.pilae.pilaeTorneo.datos.factoria.abstracta.FactoriaDAO;
import com.pilae.pilaetorneo.dominio.CategoriaDominio;
import com.pilae.pilaetorneo.dominio.DeporteDominio;
import com.pilae.pilaetorneo.dto.CategoriaDTO;
import com.pilae.pilaetorneo.negocio.negocio.ICategoriaNegocio;
import com.pilae.pilaetorneo.negocio.negocio.ensamblador.concreta.CategoriaEnsamblador;
import com.pilae.pilaetorneo.negocio.negocio.ensamblador.concreta.DeporteEnsamblador;
import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;

public class CategoriaNegocio implements ICategoriaNegocio {

	private final FactoriaDAO factoria;
	
	public CategoriaNegocio(final FactoriaDAO factoria) {
		super();
		
		if(obtenerUtilObjeto().objetoEsNulo(factoria)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando realizar la operacin deseada sobre una categoria";
			final String mensajeTecnico = "No es posible crear un CategoriaNegocio con una factoria nula.";

			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		this.factoria = factoria;
	}
	
	
	@Override
	public void crear(CategoriaDTO categoria) {
		
		// 1. Ensamblar dominio
		final CategoriaDominio categoriaDominio = CategoriaEnsamblador.obtenercategoriaEnsamblador().ensamblarDominio(categoria, OperacionEnum.CREAR);
		
		// 2. Regla Uno: Validar que no exista una Categoria con el mismo nombre Y el mismo a�o
		final CategoriaDominio validarReglaUno = CategoriaDominio.crear(0, categoriaDominio.getNombre(), null, null, categoriaDominio.getDeporte(), categoriaDominio.getAnio(), OperacionEnum.CONSULTAR);
		
		final List<CategoriaDominio> registrosReglaUno = factoria.obtenerCategoriaDAO().consultar(validarReglaUno);
		
		if(!registrosReglaUno.isEmpty()) {
			final String mensajeUsuario = "No es posible ingresar la categoria para el deporte "+registrosReglaUno.get(0).getDeporte().getNombre() +", pues ya existe esta categoria para el a�o "+ categoriaDominio.getAnio();
			final String mensajeTecnico =  "NO pueden haber dos Categorias con el mismo nombre Y el mismo deporte Y el mismo a�o.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		//3. Regla Dos: Validar que el Deporte asociado a la categoria dominio tambi�n exista
		//NOTA: Tenga en cuenta que podr� buscar al Deporte por nombre Y por id, o sea que si no coinciden, no se ensamblar�
		final DeporteDominio validarReglaDos = DeporteEnsamblador.obtenerDeporteEnsamblador().ensamblarDominio(categoria.getDeporte(), OperacionEnum.CONSULTAR);
		final List<DeporteDominio> registrosReglaDos = factoria.obtenerDeporteDAO().consultar(validarReglaDos);
		
		if(registrosReglaDos.isEmpty()) {
			final String mensajeUsuario = "No es posible asociar una Categoria a un deporte no existente.";
			final String mensajeTecnico = "No se encontr� el id_deporte, es obligatorio que la categor�a tenga un deporte existente.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		// 4. Crear la categor�a en la fuente de informaci�n.
		factoria.obtenerCategoriaDAO().crear(categoriaDominio);
	}

	@Override
	public void actualizar(CategoriaDTO categoria) {
		
		//  1. Ensamblar dominio
		final CategoriaDominio categoriaDominio = CategoriaEnsamblador.obtenercategoriaEnsamblador().ensamblarDominio(categoria, OperacionEnum.ELIMINAR);
		
		// 2. Validar que exista el id_categoria de la Categoria ingresada
		final CategoriaDominio validarReglaUno = CategoriaDominio.crear(categoriaDominio.getIdCategoria(), "", null, null, null, 0, OperacionEnum.CONSULTAR);
		final List<CategoriaDominio> registrosReglaUno = factoria.obtenerCategoriaDAO().consultar(validarReglaUno);
		if(registrosReglaUno.isEmpty()) {
			final String mensajeUsuario = "No es posible eliminar una categoria no existente";
			final String mensajeTecnico = "No se puede eliminar el Deporte, ya que el id_categoria no existe";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		// 3. Regla Dos: No exista otra Categoria con el nombre del mismo deporte en el a�o en cuestion
		final CategoriaDominio validarReglaDos = CategoriaDominio.crear(0, categoriaDominio.getNombre(), null, null, categoriaDominio.getDeporte(), categoriaDominio.getAnio(), OperacionEnum.CONSULTAR);
		
		final List<CategoriaDominio> registrosReglaDos = factoria.obtenerCategoriaDAO().consultar(validarReglaDos);
		
		if(!registrosReglaDos.isEmpty()) {
			final String mensajeUsuario = "No es posible actualizar la categoria para el deporte "+registrosReglaDos.get(0).getDeporte().getNombre() +", pues ya existe esta categoria para el a�o "+ categoriaDominio.getAnio();
			final String mensajeTecnico =  "No es posible actualizar los datos debido a que no se pueden repetir categoria en el mismo deporte y el mismo a�o.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		// 4. Lo actualizo si cambiaron los datos
		if(validarReglaUno.getNombre() != categoria.getNombre() || !validarReglaUno.getFechaNacimientoMaxima().equals(categoria.getFechaNacimientoMaxima()) 
				|| !validarReglaUno.getFechaNacimientoMinimo().equals(categoria.getFechaNacimientoMinimo()) || 
				validarReglaUno.getDeporte().getIdDeporte() != categoria.getDeporte().getIdDeporte() || validarReglaUno.getAnio() != categoria.getAnio()) {
			
			factoria.obtenerCategoriaDAO().actualizar(categoriaDominio);
		}
		
	}

	@Override
	public void eliminar(CategoriaDTO categoria) {
		
		//  1. Ensamblar dominio
		final CategoriaDominio categoriaDominio = CategoriaEnsamblador.obtenercategoriaEnsamblador().ensamblarDominio(categoria, OperacionEnum.ELIMINAR);
		
		// 2. Validar que exista el id_categoria de la Categoria ingresada
		final CategoriaDominio validarReglaUno = CategoriaDominio.crear(categoria.getIdCategoria(), "", null, null, null, 0, OperacionEnum.CONSULTAR);
		final List<CategoriaDominio> registrosReglaUno = factoria.obtenerCategoriaDAO().consultar(validarReglaUno);
		if(registrosReglaUno.isEmpty()) {
			final String mensajeUsuario = "No es posible eliminar una categoria no existente";
			final String mensajeTecnico = "No se puede eliminar el Deporte, ya que el id_categoria no existe";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		// 3. Validar que esa entidad no se est� usando en ning�n otro lugar como llave for�nea
		
		// 4. Eliminar el deporte de la fuente de informaci�n
		factoria.obtenerCategoriaDAO().eliminar(categoriaDominio);
	}

	@Override
	public List<CategoriaDTO> consultar(CategoriaDTO categoria) {
		
		// 1. Ensamblar dominio
		final CategoriaDominio categoriaDominio = CategoriaEnsamblador.obtenercategoriaEnsamblador().ensamblarDominio(categoria, OperacionEnum.CONSULTAR);
		
		
		// 2. Crear la Categoria en la fuente de informaci�n
		final List<CategoriaDominio> listaDominios = factoria.obtenerCategoriaDAO().consultar(categoriaDominio);
		
		
		return CategoriaEnsamblador.obtenercategoriaEnsamblador().enmsablarListaDTO(listaDominios);
	}

}
