package com.pilae.pilaetorneo.negocio.negocio.concreta;

import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;

import java.util.List;

import com.pilae.pilaeTorneo.datos.factoria.abstracta.FactoriaDAO;
import com.pilae.pilaetorneo.dominio.PatrocinadorDominio;
import com.pilae.pilaetorneo.dto.PatrocinadorDTO;
import com.pilae.pilaetorneo.negocio.negocio.IPatrocinadorNegocio;
import com.pilae.pilaetorneo.negocio.negocio.ensamblador.concreta.PatrocinadorEnsamblador;
import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;

public class PatrocinadorNegocio implements IPatrocinadorNegocio {
	private final FactoriaDAO factoria;
	
	public PatrocinadorNegocio(final FactoriaDAO factoria) {
		super();
		
		if(obtenerUtilObjeto().objetoEsNulo(factoria)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando realizar la operacin deseada sobre un patrocinador";
			final String mensajeTecnico = "No es posible crear un patrocinadorNegocio con una factoria nula.";

			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		this.factoria = factoria;
	}
	
	public final void crear(PatrocinadorDTO patrocinador) {
		
		// 1. Ensamblar dominio
		final PatrocinadorDominio patrocinadorDominio = PatrocinadorEnsamblador.obtenerPatrocinadorEnsamblador().ensamblarDominio(patrocinador, OperacionEnum.CREAR);
		
		// 2. Regla Uno: Validar que no exista un patrocinador con el nombre deseado
		final PatrocinadorDominio validarReglaUno = PatrocinadorDominio.crear(0, patrocinadorDominio.getNombre(), "", OperacionEnum.CONSULTAR);
		final List<PatrocinadorDominio> registrosReglaUno = factoria.obtenerPatrocinadorDAO().consultar(validarReglaUno);
		if(!registrosReglaUno.isEmpty()) {
			final String mensajeUsuario = "Ya existe un patrocinador con el nombre ingresado";
			final String mensajeTecnico = "No se pueden registar dos patrocinadors con el mismo nombre";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		// 3. Crear el patrocinador en la fuente de informaci�n
		factoria.obtenerPatrocinadorDAO().crear(patrocinadorDominio);
	}

	@Override
	public final void actualizar(PatrocinadorDTO patrocinador) {
		
		// 1. Ensamblar dominio
		final PatrocinadorDominio patrocinadorDominio = PatrocinadorEnsamblador.obtenerPatrocinadorEnsamblador().ensamblarDominio(patrocinador, OperacionEnum.ACTUALIZAR);
		
		// 2. Regla Uno: Validar que exista un patrocinador con el nombre deseado
		final PatrocinadorDominio validarReglaUno = PatrocinadorDominio.crear(patrocinador.getIdPatrocinador(), "", "", OperacionEnum.CONSULTAR);
		final List<PatrocinadorDominio> registrosReglaUno = factoria.obtenerPatrocinadorDAO().consultar(validarReglaUno);
		
		if(registrosReglaUno.isEmpty()) {
			final String mensajeUsuario = "No es posible actualizar un patrocinador no existente";
			final String mensajeTecnico = "No se puede actualizar el patrocinador, ya que el id_patrocinador no existe";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		// 3. Regla Dos: No exista otro patrocinador con el nombre ingresado.
		final PatrocinadorDominio validarReglaDos = PatrocinadorDominio.crear(0, patrocinador.getNombre(), "", OperacionEnum.CONSULTAR);
		final List<PatrocinadorDominio> registrosReglaDos = factoria.obtenerPatrocinadorDAO().consultar(validarReglaDos);
		
		if(!registrosReglaDos.isEmpty() && registrosReglaDos.get(0).getIdPatrocinador() != patrocinador.getIdPatrocinador()) {
			final String mensajeUsuario = "Ya existe un patrocinador con el nombre ingresado";
			final String mensajeTecnico = "No se pueden resgistrar 2 patrocinadors con el mismo nombre";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		// 4. Lo actualizo si cambiaron los datos
		if(!registrosReglaUno.get(0).getNombre().equals(patrocinadorDominio.getNombre())
				||  !registrosReglaUno.get(0).getDescripcion().equals(patrocinadorDominio.getDescripcion())) {
			
			factoria.obtenerPatrocinadorDAO().actualizar(patrocinadorDominio);
		}		
	}

	@Override
	public final void eliminar(PatrocinadorDTO patrocinador) {
		
		// 1. Ensamblar dominio
		final PatrocinadorDominio patrocinadorDominio = PatrocinadorEnsamblador.obtenerPatrocinadorEnsamblador().ensamblarDominio(patrocinador, OperacionEnum.ELIMINAR);
		
		// 2. Regla Uno: Validar que exista un patrocinador con el nombre deseado
		final PatrocinadorDominio validarReglaUno = PatrocinadorDominio.crear(patrocinador.getIdPatrocinador(), "", "", OperacionEnum.CONSULTAR);
		final List<PatrocinadorDominio> registrosReglaUno = factoria.obtenerPatrocinadorDAO().consultar(validarReglaUno);
		if(registrosReglaUno.isEmpty()) {
			final String mensajeUsuario = "No es posible eliminar un patrocinador no existente";
			final String mensajeTecnico = "No se puede eliminar el patrocinador, ya que el id_patrocinador no existe";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
		}
		
		// 3. Validar que esa entidad no se est� usando en ning�n otro lugar como llave for�nea
		
		// 4. Eliminar el patrocinador de la fuente de informaci�n
		factoria.obtenerPatrocinadorDAO().eliminar(patrocinadorDominio);
	}

	@Override
	public final List<PatrocinadorDTO> consultar(PatrocinadorDTO patrocinador) {
		
		// 1. Ensamblar dominio
		final PatrocinadorDominio patrocinadorDominio = PatrocinadorEnsamblador.obtenerPatrocinadorEnsamblador().ensamblarDominio(patrocinador, OperacionEnum.CONSULTAR);
		
		// 2. Crear el tipo de cuenta en la fuente de informaci�n
		final List<PatrocinadorDominio> listaDominios = factoria.obtenerPatrocinadorDAO().consultar(patrocinadorDominio);
		return PatrocinadorEnsamblador.obtenerPatrocinadorEnsamblador().enmsablarListaDTO(listaDominios);
	}
}
