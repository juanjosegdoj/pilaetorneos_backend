package com.pilae.pilaetorneo.dto;

import static com.pilae.pilaetorneo.utilitarios.cadenas.UtilTexto.obtenerUtilTexto;

public final class RolDTO {
	private String idRol;
	private String nombre;
	private String descripcion;
	
	public RolDTO() {
		super();
	}
	
	public RolDTO(String idRol, String nombre, String descripcion) {
		super();
		setIdRol(idRol);
		setNombre(nombre);
		setDescripcion(descripcion);
	}

	public final String getIdRol() {
		return idRol;
	}

	public final void setIdRol(String idRol) {
		this.idRol = idRol;
	}

	public final String getNombre() {
		return nombre;
	}

	public final void setNombre(String nombre) {
		this.nombre = obtenerUtilTexto().aplicarTrim(nombre);
	}

	public final String getDescripcion() {
		return descripcion;
	}

	public final void setDescripcion(String descripcion) {
		this.descripcion = obtenerUtilTexto().aplicarTrim(descripcion);
	}
	
	
}
