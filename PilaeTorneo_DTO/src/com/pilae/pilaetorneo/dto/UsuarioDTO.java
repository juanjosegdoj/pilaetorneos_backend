package com.pilae.pilaetorneo.dto;

public final class UsuarioDTO {
	private int idUsuario;
	private GeneroDTO genero;
	
	public UsuarioDTO(int idUsuario, GeneroDTO genero) {
		super();
		this.idUsuario = idUsuario;
		this.genero = genero;
	}
	
	public final int getIdUsuario() {
		return idUsuario;
	}
	public final void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public final GeneroDTO getGenero() {
		return genero;
	}
	public final void setGenero(GeneroDTO genero) {
		this.genero = genero;
	}	
	
	
}
