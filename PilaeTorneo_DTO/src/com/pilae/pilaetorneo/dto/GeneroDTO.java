package com.pilae.pilaetorneo.dto;

import static com.pilae.pilaetorneo.utilitarios.cadenas.UtilTexto.obtenerUtilTexto;

public final class GeneroDTO {
	
	private int idGenero;
	private String nombre;

	public GeneroDTO() {
		super();
	}
	
	public GeneroDTO(int idGenero, String nombre) {
		super();
		setIdGenero(idGenero);
		setNombre(nombre);
	}
	
	public final int getIdGenero() {
		return idGenero;
	}

	public final void setIdGenero(int idGenero) {
		this.idGenero = idGenero;
	}

	public final String getNombre() {
		return nombre;
	}

	public final void setNombre(String nombre) {
		this.nombre = obtenerUtilTexto().aplicarTrim(nombre);
	}
	
}
