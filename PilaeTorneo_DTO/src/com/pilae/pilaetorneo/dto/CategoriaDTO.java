package com.pilae.pilaetorneo.dto;


import com.fasterxml.jackson.annotation.JsonFormat;

import static com.pilae.pilaetorneo.utilitarios.cadenas.UtilTexto.obtenerUtilTexto;
import static com.pilae.pilaetorneo.utilitarios.fecha.UtilFecha.obtenerUtilFecha;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public final class CategoriaDTO {
	
	private int idCategoria;
	private String nombre;
	
	@Temporal(TemporalType.DATE) @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es-CO", timezone = "America/Bogota")
	private Date fechaNacimientoMaxima;
	
	@Temporal(TemporalType.DATE) @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es-CO", timezone = "America/Bogota")
	private Date fechaNacimientoMinimo;
	
	private DeporteDTO deporte;
	private int anio;
	
	public CategoriaDTO() {
		super();
	}
	
	public CategoriaDTO(int idCategoria, String nombre, final Date fechaNacimientoMaxima,
			final Date fechaNacimientoMinimo, DeporteDTO deporte, int anio) {
		
		super();
		setIdCategoria(idCategoria);
		setNombre(nombre);
		setFechaNacimientoMaxima(fechaNacimientoMaxima);
		setFechaNacimientoMinimo(fechaNacimientoMinimo);
		setDeporte(deporte);
		setAnio(anio);
	}
	
	public final int getIdCategoria() {
		return idCategoria;
	}

	public final void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}

	public final String getNombre() {
		return nombre;
	}

	public final void setNombre(String nombre) {
		this.nombre = obtenerUtilTexto().aplicarTrim(nombre);
	}

	public final Date getFechaNacimientoMaxima() {
		return fechaNacimientoMaxima;
	}

	public final void setFechaNacimientoMaxima(Date fechaNacimientoMaxima) {
		this.fechaNacimientoMaxima = fechaNacimientoMaxima;
	}

	public final Date getFechaNacimientoMinimo() {
		return fechaNacimientoMinimo;
	}

	public final void setFechaNacimientoMinimo(Date fechaNacimientoMinimo) {
		this.fechaNacimientoMinimo = fechaNacimientoMinimo;
	}

	public final DeporteDTO getDeporte() {
		return deporte;
	}

	public final void setDeporte(DeporteDTO deporte) {
		this.deporte = deporte;
	}
	public final int getAnio() {
		return anio;
	}

	public final void setAnio(int anio) {
		this.anio = anio;
	}
}
