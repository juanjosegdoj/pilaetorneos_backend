package com.pilae.pilaetorneo.dto;

import static com.pilae.pilaetorneo.utilitarios.cadenas.UtilTexto.obtenerUtilTexto;

public final class EstadoDTO {
	private int idEstado;
	private String nombre;
	private String descripcion;
	
	public EstadoDTO() {
		super();
	}
	
	public EstadoDTO(int idEstado, String nombre, String descripcion) {
		super();
		setIdEstado(idEstado);
		setNombre(nombre);
		setDescripcion(descripcion);
	}

	public final int getIdEstado() {
		return idEstado;
	}

	public final void setIdEstado(int idEstado) {
		this.idEstado = idEstado;
	}

	public final String getNombre() {
		return nombre;
	}

	public final void setNombre(String nombre) {
		this.nombre = obtenerUtilTexto().aplicarTrim(nombre);
	}

	public final String getDescripcion() {
		return descripcion;
	}

	public final void setDescripcion(String descripcion) {
		this.descripcion = obtenerUtilTexto().aplicarTrim(descripcion);
	}
	
}