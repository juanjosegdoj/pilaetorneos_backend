package com.pilae.pilaetorneo.dto;

import static com.pilae.pilaetorneo.utilitarios.cadenas.UtilTexto.obtenerUtilTexto;

public final class DeporteDTO {
	
	private int idDeporte;
	private String nombre;
	private String descripcion;

	public DeporteDTO() {
		super();
	}
	
	public DeporteDTO(int idDeporte, String nombre, String descripcion) {
		super();
		setIdDeporte(idDeporte);
		setNombre(nombre);
		setDescripcion(descripcion);
	}

	public final int getIdDeporte() {
		return idDeporte;
	}

	private final void setIdDeporte(int idDeporte) {
		this.idDeporte = idDeporte;
	}

	public final String getNombre() {
		return nombre;
	}

	private final void setNombre(String nombre) {
		this.nombre = obtenerUtilTexto().aplicarTrim(nombre);
	}

	public final String getDescripcion() {
		return descripcion;
	}

	private final void setDescripcion(String descripcion) {
		this.descripcion = obtenerUtilTexto().aplicarTrim(descripcion);
	}
	
}