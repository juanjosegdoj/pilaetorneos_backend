package com.pilae.pilaetorneo.dto;

import java.util.Date;
import java.util.List;

import static com.pilae.pilaetorneo.utilitarios.cadenas.UtilTexto.obtenerUtilTexto;

public final class TorneoDTO {
	
	private int idTorneo;
	private String nombre;
	private String descripcion;
	private Date fechaInicio;
	private Date fechaFin;
	private GeneroDTO genero;
	private List<EquipoDTO> equipos;
	private UsuarioDTO administrador;
	private String premiacion;
	private CategoriaDTO categoriaDeDeporte;
	private String imagen;
	private EstadoDTO estado;
	
	public TorneoDTO() {
		super();
	}
	
	public TorneoDTO(int idTorneo, String nombre, String descripcion, Date fechaInicio, Date fechaFin, GeneroDTO genero,
			List<EquipoDTO> equipos, UsuarioDTO administrador, String premiacion,
			CategoriaDTO categoriaDeDeporte, String imagen, EstadoDTO estado) {
		super();
		setIdTorneo(idTorneo);
		setNombre(nombre);
		setDescripcion(descripcion);
		setFechaInicio(fechaInicio);
		setFechaFin(fechaFin);
		setGenero(genero);
		setEquipos(equipos);
		setAdministrador(administrador);
		setPremiacion(premiacion);
		setCategoriaDeDeporte(categoriaDeDeporte);
		setImagen(imagen);
		setEstado(estado);
	}

	public final int getIdTorneo() {
		return idTorneo;
	}

	public final void setIdTorneo(int idTorneo) {
		this.idTorneo = idTorneo;
	}

	public final String getNombre() {
		return nombre;
	}

	public final void setNombre(String nombre) {
		this.nombre = obtenerUtilTexto().aplicarTrim(nombre);
	}

	public final String getDescripcion() {
		return descripcion;
	}

	public final void setDescripcion(String descripcion) {
		this.descripcion = obtenerUtilTexto().aplicarTrim(descripcion);
	}

	public final Date getFechaInicio() {
		return fechaInicio;
	}

	public final void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public final Date getFechaFin() {
		return fechaFin;
	}

	public final void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public final GeneroDTO getGenero() {
		return genero;
	}

	public final void setGenero(GeneroDTO genero) {
		this.genero = genero;
	}

	public final List<EquipoDTO> getEquipos() {
		return equipos;
	}

	public final void setEquipos(List<EquipoDTO> equipos) {
		this.equipos = equipos;
	}

	public final UsuarioDTO getAdministrador() {
		return administrador;
	}

	public final void setAdministrador(UsuarioDTO administrador) {
		this.administrador = administrador;
	}

	public final String getPremiacion() {
		return premiacion;
	}

	public final void setPremiacion(String premiacion) {
		this.premiacion = obtenerUtilTexto().aplicarTrim(premiacion);
	}

	public final CategoriaDTO getCategoriaDeDeporte() {
		return categoriaDeDeporte;
	}

	public final void setCategoriaDeDeporte(CategoriaDTO categoriaDeDeporte) {
		this.categoriaDeDeporte = categoriaDeDeporte;
	}

	public final String getImagen() {
		return imagen;
	}

	public final void setImagen(String imagen) {
		this.imagen = obtenerUtilTexto().aplicarTrim(imagen);
	}

	public final EstadoDTO getEstado() {
		return estado;
	}

	public final void setEstado(EstadoDTO estado) {
		this.estado = estado;
	}
	
}
