package com.pilae.pilaetorneo.dto;

import static com.pilae.pilaetorneo.utilitarios.cadenas.UtilTexto.obtenerUtilTexto;

import java.util.List;

public final class EquipoDTO {
	private int idEquipo;
	private String nombre;
	private UsuarioDTO tecnico;
	private String imagen;
	private List<UsuarioDTO> jugadores;
	
	public EquipoDTO() {
		super();
	}
	
	public EquipoDTO(int idEquipo, String nombre, UsuarioDTO tecnico, String imagen, List<UsuarioDTO> jugadores) {
		super();
		setIdEquipo(idEquipo);
		setNombre(nombre);
		setTecnico(tecnico);
		setJugadores(jugadores);
	}

	public final int getIdEquipo() {
		return idEquipo;
	}

	public final void setIdEquipo(int idEquipo) {
		this.idEquipo = idEquipo;
	}

	public final String getNombre() {
		return nombre;
	}

	public final void setNombre(String nombre) {
		this.nombre = obtenerUtilTexto().aplicarTrim(nombre);
	}

	public final UsuarioDTO getTecnico() {
		return tecnico;
	}

	public final void setTecnico(UsuarioDTO tecnico) {
		this.tecnico = tecnico;
	}

	public final String getImagen() {
		return imagen;
	}

	public final void setImagen(String imagen) {
		this.imagen = obtenerUtilTexto().aplicarTrim(imagen);
	}

	public final List<UsuarioDTO> getJugadores() {
		return jugadores;
	}

	public final void setJugadores(List<UsuarioDTO> jugadores) {
		this.jugadores = jugadores;
	}
	
	
}
