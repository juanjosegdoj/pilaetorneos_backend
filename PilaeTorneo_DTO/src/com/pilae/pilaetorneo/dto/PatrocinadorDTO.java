package com.pilae.pilaetorneo.dto;

public class PatrocinadorDTO {
	
	private int idPatrocinador;
	private String nombre;
	private String descripcion;
	
	public PatrocinadorDTO() {
		super();
	}
	
	public PatrocinadorDTO(int idPatrocinador, String nombre, String descripcion) {
		super();
		setIdPatrocinador(idPatrocinador);
		setNombre(nombre);
		setDescripcion(descripcion);
	}
	
	public int getIdPatrocinador() {
		return idPatrocinador;
	}
	public void setIdPatrocinador( int idPatrocinador) {
		this.idPatrocinador = idPatrocinador;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre( String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public  void setDescripcion( String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
