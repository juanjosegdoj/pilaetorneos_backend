package com.pilae.pilaeTorneo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PilaeTorneoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PilaeTorneoApplication.class, args);
	}
}