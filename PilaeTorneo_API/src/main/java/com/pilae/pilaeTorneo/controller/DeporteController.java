package com.pilae.pilaeTorneo.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.pilae.pilaetorneo.negocio.fachada.concreta.DeporteFachada;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import com.pilae.pilaetorneo.dto.DeporteDTO;

@RestController
@RequestMapping("pilae/torneo")
public class DeporteController {
	
	
	@PostMapping("/deportes")
	public void crear(@RequestBody DeporteDTO deporte){
				
		try {
			DeporteFachada fachada = new DeporteFachada();
			fachada.crear(deporte);		
		} catch (AplicacionExcepcion e) {
			System.out.println(e.getMensajeTecnico());
			System.out.println(e.getMensajeUsuario());
		}
	}
	
	@PutMapping("/deportes")
	public void actualizar(@RequestBody DeporteDTO deporte) {
		try {
			DeporteFachada fachada = new DeporteFachada();
			fachada.actualizar(deporte);
		} catch (AplicacionExcepcion e) {
			System.out.println(e.getMensajeTecnico());
			System.out.println(e.getMensajeUsuario());
			System.out.println(e.getLugarExcepcion());
			System.out.println(e.getExcepcionRaiz());
		}
	}
	
	@GetMapping("/deportes")
	public List<DeporteDTO> consultar(@RequestBody DeporteDTO deporte) {
		
		List<DeporteDTO> deportes = null;
		try {
			DeporteFachada fachada = new DeporteFachada();
			deportes = fachada.consultar(deporte);	
		} catch (AplicacionExcepcion e) {
			System.out.println(e.getMensajeTecnico());
			System.out.println(e.getMensajeUsuario());
			System.out.println(e.getLugarExcepcion());
			System.out.println(e.getExcepcionRaiz());
		}
		
		return deportes;
	}
	
	@DeleteMapping("/deportes")
	public void eliminar(@RequestBody DeporteDTO deporte) {
		
		try {
			DeporteFachada fachada = new DeporteFachada();
			fachada.eliminar(deporte);	
		} catch (AplicacionExcepcion e) {
			System.out.println(e.getMensajeTecnico());
			System.out.println(e.getMensajeUsuario());
			System.out.println(e.getLugarExcepcion());
			System.out.println(e.getExcepcionRaiz());
		}
	}
}
