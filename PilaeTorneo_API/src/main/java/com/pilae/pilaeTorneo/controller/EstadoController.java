package com.pilae.pilaeTorneo.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.pilae.pilaetorneo.negocio.fachada.concreta.EstadoFachada;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import com.pilae.pilaetorneo.dto.EstadoDTO;

@RestController
@RequestMapping("pilae/torneo")
public class EstadoController {
	
	
	@PostMapping("/estados")
	public void crear(@RequestBody EstadoDTO estado){
				
		try {
			EstadoFachada fachada = new EstadoFachada();
			fachada.crear(estado);		
		} catch (AplicacionExcepcion e) {
			System.out.println(e.getMensajeTecnico());
			System.out.println(e.getMensajeUsuario());
		}
	}
	
	@PutMapping("/estados")
	public void actualizar(@RequestBody EstadoDTO estado) {
		try {
			EstadoFachada fachada = new EstadoFachada();
			fachada.actualizar(estado);	
			
		} catch (AplicacionExcepcion e) {
			System.out.println(e.getMensajeTecnico());
			System.out.println(e.getMensajeUsuario());
			System.out.println(e.getLugarExcepcion());
			System.out.println(e.getExcepcionRaiz());
			System.out.println("Si fue por el put");
		}
	}
	
	@GetMapping("/estados")
	public List<EstadoDTO> consultar(@RequestBody EstadoDTO estado) {
		
		List<EstadoDTO> estados = null;
		
		try {
			EstadoFachada fachada = new EstadoFachada();
			estados = fachada.consultar(estado);
		} catch (AplicacionExcepcion e) {
			System.out.println(e.getMensajeTecnico());
			System.out.println(e.getMensajeUsuario());
			System.out.println(e.getLugarExcepcion());
			System.out.println(e.getExcepcionRaiz());
			System.out.println(e.getExcepcionRaiz().getLocalizedMessage());
			System.out.println("---------");
		}
		
		return estados;
	}
	
	@DeleteMapping("/estados")
	public void eliminar(@RequestBody EstadoDTO estado) {
		
		try {
			EstadoFachada fachada = new EstadoFachada();
			fachada.eliminar(estado);	
		} catch (AplicacionExcepcion e) {
			System.out.println(e.getMensajeTecnico());
			System.out.println(e.getMensajeUsuario());
			System.out.println(e.getLugarExcepcion());
			System.out.println(e.getExcepcionRaiz());
		}
	}
}