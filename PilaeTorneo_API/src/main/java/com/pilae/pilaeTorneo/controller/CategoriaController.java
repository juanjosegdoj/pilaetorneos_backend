package com.pilae.pilaeTorneo.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pilae.pilaetorneo.dto.CategoriaDTO;
import com.pilae.pilaetorneo.negocio.fachada.concreta.CategoriaFachada;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;

@RestController
@RequestMapping("pilae/torneo")
public class CategoriaController {
	
	// CUIDADO: Se estalla cuando le mando cosas como esta: "fechaNacimientoMinimo": "2012-1-01"

	@PostMapping("/categoria")
	public void crear(@RequestBody CategoriaDTO categoria){
		
		try {
			CategoriaFachada fachada = new CategoriaFachada();
			fachada.crear(categoria);		
				
		} catch (AplicacionExcepcion e) {
			System.out.println(e.getMensajeTecnico());
			System.out.println(e.getMensajeUsuario());
			System.out.println(e.getLugarExcepcion());
			System.out.println(e.getExcepcionRaiz());
		}
	}
	
	@PutMapping("/categoria")
	public void actualizar(@RequestBody CategoriaDTO categoria) {
		try {
			CategoriaFachada fachada = new CategoriaFachada();
			fachada.actualizar(categoria);
		} catch (AplicacionExcepcion e) {
			System.out.println(e.getMensajeTecnico());
			System.out.println(e.getMensajeUsuario());
			System.out.println(e.getLugarExcepcion());
			System.out.println(e.getExcepcionRaiz());
		}
	}
	
	@GetMapping("/categorias")
	public List<CategoriaDTO> consultar(@RequestBody CategoriaDTO categoria) {
		
		List<CategoriaDTO> categorias = null;
		
		try {
			CategoriaFachada fachada = new CategoriaFachada();
			categorias = fachada.consultar(categoria);	
		} catch (AplicacionExcepcion e) {
			System.out.println(e.getMensajeTecnico());
			System.out.println(e.getMensajeUsuario());
			System.out.println(e.getLugarExcepcion());
			System.out.println(e.getExcepcionRaiz());
		}
		
		return categorias;
	}
	
	@DeleteMapping("/categoria")
	public void eliminar(@RequestBody CategoriaDTO categoria) {
		
		try {
			CategoriaFachada fachada = new CategoriaFachada();
			fachada.eliminar(categoria);	
		} catch (AplicacionExcepcion e) {
			System.out.println(e.getMensajeTecnico());
			System.out.println(e.getMensajeUsuario());
			System.out.println(e.getLugarExcepcion());
			System.out.println(e.getExcepcionRaiz());
		}
	}
}
