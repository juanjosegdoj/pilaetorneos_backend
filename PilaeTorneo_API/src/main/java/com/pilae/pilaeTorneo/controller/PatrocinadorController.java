package com.pilae.pilaeTorneo.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.pilae.pilaetorneo.negocio.fachada.concreta.PatrocinadorFachada;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import com.pilae.pilaetorneo.dto.PatrocinadorDTO;

@RestController
@RequestMapping("pilae/torneo")
public class PatrocinadorController {
	
	@PostMapping("/patrocinadores")
	public void crear(@RequestBody PatrocinadorDTO patrocinador){
				
		try {
			PatrocinadorFachada fachada = new PatrocinadorFachada();
			fachada.crear(patrocinador);		
		} catch (AplicacionExcepcion e) {
			System.out.println(e.getMensajeTecnico());
			System.out.println(e.getMensajeUsuario());
			System.out.println(e.getLugarExcepcion());
			System.out.println(e.getExcepcionRaiz());
		}
	}
	
	@PutMapping("/patrocinadores")
	public void actualizar(@RequestBody PatrocinadorDTO patrocinador) {
		try {
			PatrocinadorFachada fachada = new PatrocinadorFachada();
			fachada.actualizar(patrocinador);
		} catch (AplicacionExcepcion e) {
			System.out.println(e.getMensajeTecnico());
			System.out.println(e.getMensajeUsuario());
			System.out.println(e.getLugarExcepcion());
			System.out.println(e.getExcepcionRaiz());
		}
	}
	
	@GetMapping("/patrocinadores")
	public List<PatrocinadorDTO> consultar(@RequestBody PatrocinadorDTO patrocinador) {
		
		List<PatrocinadorDTO> patrocinadores = null;
		try {
			PatrocinadorFachada fachada = new PatrocinadorFachada();
			patrocinadores = fachada.consultar(patrocinador);	
		} catch (AplicacionExcepcion e) {
			System.out.println(e.getMensajeTecnico());
			System.out.println(e.getMensajeUsuario());
			System.out.println(e.getLugarExcepcion());
			System.out.println(e.getExcepcionRaiz());
		}
		
		return patrocinadores;
	}
	
	@DeleteMapping("/patrocinadores")
	public void eliminar(@RequestBody PatrocinadorDTO patrocinador) {
		
		try {
			PatrocinadorFachada fachada = new PatrocinadorFachada();
			fachada.eliminar(patrocinador);	
		} catch (AplicacionExcepcion e) {
			System.out.println(e.getMensajeTecnico());
			System.out.println(e.getMensajeUsuario());
			System.out.println(e.getLugarExcepcion());
			System.out.println(e.getExcepcionRaiz());
		}
	}
}
