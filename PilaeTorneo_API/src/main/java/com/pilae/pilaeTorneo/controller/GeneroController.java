package com.pilae.pilaeTorneo.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pilae.pilaetorneo.dto.GeneroDTO;
import com.pilae.pilaetorneo.negocio.fachada.concreta.GeneroFachada;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;

@RestController
@RequestMapping("pilae/torneo")
public class GeneroController {
	
	@PostMapping("/generos")
	public void crear(@RequestBody GeneroDTO genero) {
		try {
			GeneroFachada fachada = new GeneroFachada();
			fachada.crear(genero);			
		} catch (AplicacionExcepcion e) {
			System.out.println(e.getMensajeTecnico());
			System.out.println(e.getMensajeUsuario());
			System.out.println(e.getLugarExcepcion());
			System.out.println(e.getExcepcionRaiz());
		}

	}
	
	@PutMapping("/generos")
	public void actualizar(@RequestBody GeneroDTO genero) {
		try {
			GeneroFachada fachada = new GeneroFachada();
			fachada.actualizar(genero);			
		} catch (AplicacionExcepcion e) {
			System.out.println(e.getMensajeTecnico());
			System.out.println(e.getMensajeUsuario());
			System.out.println(e.getLugarExcepcion());
			System.out.println(e.getExcepcionRaiz());
		}

	}
	
	@GetMapping("/generos")
	public List<GeneroDTO> consultar(@RequestBody GeneroDTO genero) {
		GeneroFachada fachada = new GeneroFachada();
		return fachada.consultar(genero);
	}
	
	@DeleteMapping("/generos")
	public void eliminar(@RequestBody GeneroDTO genero) {
		try {
			GeneroFachada fachada = new GeneroFachada();
			fachada.eliminar(genero);			
		} catch (AplicacionExcepcion e) {
			System.out.println(e.getMensajeTecnico());
			System.out.println(e.getMensajeUsuario());
			System.out.println(e.getLugarExcepcion());
			System.out.println(e.getExcepcionRaiz());
		}
	}
}
