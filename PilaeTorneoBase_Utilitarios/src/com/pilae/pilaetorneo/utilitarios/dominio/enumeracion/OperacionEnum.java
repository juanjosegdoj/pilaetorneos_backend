package com.pilae.pilaetorneo.utilitarios.dominio.enumeracion;

public enum OperacionEnum {
	
	CREAR, ACTUALIZAR, ELIMINAR, CONSULTAR, POBLAR, DEPENDENCIA;
	
}