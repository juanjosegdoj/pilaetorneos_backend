package com.pilae.pilaetorneo.utilitarios.excepcion.excepcion;

import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;

import static com.pilae.pilaetorneo.utilitarios.cadenas.UtilTexto.obtenerUtilTexto;


public final class AplicacionExcepcion extends RuntimeException {

	/**
	 * Serial Version por defecto de la clase
	 */
	private static final long serialVersionUID = 1L;

	private String mensajeTecnico;
	private String mensajeUsuario;
	private Exception excepcionRaiz;
	private boolean existeExcepcioneRaiz;
	private ExcepcionEnum lugarExcepcion;

	private AplicacionExcepcion(final String mensajeTecnico, final String mensajeUsuario, final Exception excepcionRaiz,
			final ExcepcionEnum lugarExcepcion) {
		super();
		setMensajeTecnico(mensajeTecnico);
		setMensajeUsuario(mensajeUsuario);
		setExcepcionRaiz(excepcionRaiz);
		setLugarExcepcion(lugarExcepcion);
	}
	
	public static AplicacionExcepcion CREAR(final String mensajeTecnico, 
			final String mensajeUsuario, 
			final Exception excepcionRaiz, 
			final ExcepcionEnum lugarExcepcion) {
		return new AplicacionExcepcion(mensajeTecnico, mensajeUsuario, excepcionRaiz, lugarExcepcion);
	}
	
	public static AplicacionExcepcion CREAR(final String mensajeUsuario, 
			final Exception excepcionRaiz,
			final ExcepcionEnum lugarExcepcion) {
		return new AplicacionExcepcion(mensajeUsuario, mensajeUsuario, excepcionRaiz, lugarExcepcion);
	}
	
	public static AplicacionExcepcion CREAR(final String mensajeTecnico, 
			final String mensajeUsuario,  
			final ExcepcionEnum lugarExcepcion) {
		return new AplicacionExcepcion(mensajeTecnico, mensajeUsuario, null, lugarExcepcion);
	}
	
	public static AplicacionExcepcion CREAR(final String mensajeUsuario, final ExcepcionEnum lugarExcepcion) {	
		return new AplicacionExcepcion(mensajeUsuario, mensajeUsuario, null, lugarExcepcion);
	}
	
	public static AplicacionExcepcion CREAR(final String mensajeUsuario) {	
		return new AplicacionExcepcion(mensajeUsuario, mensajeUsuario, null, null);
	}
	
	private final void setMensajeTecnico(String mensajeTecnico) {
		this.mensajeTecnico = obtenerUtilTexto().aplicarTrim(mensajeTecnico);
	}
	private final void setMensajeUsuario(String mensajeUsuario) {
		this.mensajeUsuario = obtenerUtilTexto().aplicarTrim(mensajeUsuario);
	}
	private final void setExcepcionRaiz(Exception excepcionRaiz) {
		setExisteExcepcioneRaiz(!obtenerUtilObjeto().objetoEsNulo(excepcionRaiz));
		this.excepcionRaiz = obtenerUtilObjeto().obtenerValorDefecto(excepcionRaiz, new Exception());
	}
	private final void setExisteExcepcioneRaiz(boolean existeExcepcioneRaiz) {
		this.existeExcepcioneRaiz = existeExcepcioneRaiz;
	}
	private final void setLugarExcepcion(ExcepcionEnum lugarExcepcion) {
		this.lugarExcepcion = obtenerUtilObjeto().obtenerValorDefecto(lugarExcepcion, ExcepcionEnum.GENERAL);
	}
	
	public final String getMensajeTecnico() {
		return mensajeTecnico;
	}
	public final String getMensajeUsuario() {
		return mensajeUsuario;
	}
	public final Exception getExcepcionRaiz() {
		return excepcionRaiz;
	}
	public final boolean isExisteExcepcioneRaiz() {
		return existeExcepcioneRaiz;
	}
	public final ExcepcionEnum getLugarExcepcion() {
		return lugarExcepcion;
	}
	
}
