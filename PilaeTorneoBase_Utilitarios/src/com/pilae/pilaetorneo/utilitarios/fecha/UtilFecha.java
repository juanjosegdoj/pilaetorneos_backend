package com.pilae.pilaetorneo.utilitarios.fecha;

/**
 * asegurarNoNulidad() Se encarga de no me permitir nulos dentro de nuestra aplicacion
 * y en caso de que nos quieran ingresar algun nulo, pone un valor por defecto alternativo
 * que no interrumpa nuestra l�gica de negocio
 */


import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto;

public class UtilFecha {
	
	private static final UtilFecha INSTANCIA = new UtilFecha();
	
	private UtilFecha() {
		super();
	}
	
	public static final UtilFecha obtenerUtilFecha() {
		return INSTANCIA;
	}
	
	public Date getFechaMinima() {
		
		Calendar fechaMinima = Calendar.getInstance();
		fechaMinima.set(
				fechaMinima.getMinimum(Calendar.YEAR),
				fechaMinima.getMinimum(Calendar.MONTH),
				fechaMinima.getMinimum(Calendar.DAY_OF_MONTH)
				);
		
		Date salida = new Date();
		salida.setTime(fechaMinima.getTime().getTime());
		return salida;
	}
	
	public Date asegurarNoNulidad(Date fecha) {
		return obtenerUtilObjeto().obtenerValorDefecto(fecha, getFechaMinima());
	}
	
	public Date fechaActual() {
		return new Date();
	}
	
	public Date stringADate(String dateStr) throws ParseException {
		Date retorno = null;
		
		if(!obtenerUtilObjeto().objetoEsNulo(dateStr)) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			format.setLenient(false);
			retorno = format.parse(dateStr); 
		}
		
		return retorno;
	}
	
	
	public String dateAString(Date date) {
		String retorno = null;
		if(!UtilObjeto.obtenerUtilObjeto().objetoEsNulo(date)) {
			SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
			retorno = formato.format(date);
		}
		return retorno;
	}
	
}
