package com.pilae.pilaetorneo.utilitarios.sql;

import java.sql.Connection;
import java.sql.SQLException;

import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;

import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;


public class UtilSQL {
	private static final UtilSQL INSTANCIA = new UtilSQL();
	
	private UtilSQL() {
		super();
	}
	
	public static final UtilSQL obtenerUtilSQL() {
		return INSTANCIA;
	}
	
	public final boolean conexionEstaAbierta(final Connection conexion) {
		try {
			return !obtenerUtilObjeto().objetoEsNulo(conexion) && !conexion.isClosed();
		}catch (final SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de valida la conexi�n contra la fuente de informaci�n";
			final String mensajeTecnico = "Se ha presentado un problema al intentar validar si la conexi�n est� abierta contra la fuente de informacion, por favor revise la traza de la excepcion";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.DATOS);
		}catch (final Exception excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de valida la conexi�n contra la fuente de informaci�n";
			final String mensajeTecnico = "Se ha presentado un problema inesperado al intentar validar si la conexi�n est� abierta contra la fuente de informacion, por favor revise la traza de la excepcion";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.DATOS);			
		}
	}
}