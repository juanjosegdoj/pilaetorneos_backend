package com.pilae.pilaetorneo.utilitarios.objeto;

/**
 * Objeto utilitario que puede ser utilizado de forma gen�rica  por alg�n objeto que 
 * la requiere, asegurando que no hayan nulos, y en caso de haberlos poner un 
 * objeto por defecto
 * @author Usuario
 *
 */

public final class UtilObjeto {
	
	private static final UtilObjeto INSTANCIA = new UtilObjeto();
	
	private UtilObjeto() {
		super();
	}
	
	public static final UtilObjeto obtenerUtilObjeto() {
		return INSTANCIA;
	}
	
	public <T> T obtenerValorDefecto(T objeto, T valorDefecto) {	
		T retorno = objeto;
		
		if(objeto == null) {
			retorno = valorDefecto;
		}
		
		return retorno;
	}
	
	public <T> boolean objetoEsNulo(T objeto) {
		return (objeto == null);
	}
}