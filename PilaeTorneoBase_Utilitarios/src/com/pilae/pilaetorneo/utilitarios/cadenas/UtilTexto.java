package com.pilae.pilaetorneo.utilitarios.cadenas;

import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;


public final class UtilTexto {

	private static final UtilTexto INSTANCIA = new UtilTexto();
	
	private UtilTexto() {
		super();
	}
	
	public static final UtilTexto obtenerUtilTexto() {
		return INSTANCIA;
	}
	
	public String aplicarTrim(String cadena) {
		return obtenerUtilObjeto().obtenerValorDefecto(cadena, "").trim();
	}
	
	public final boolean cadenaEsVaciaONula(final String cadena) {
		return aplicarTrim(cadena).intern() == "" ;
	}

}