package com.pilae.pilaeTorneo.datos.factoria.abstracta;

import com.pilae.pilaeTorneo.datos.dao.interfaces.ICategoriaDAO;
import com.pilae.pilaeTorneo.datos.dao.interfaces.IDeporteDAO;
import com.pilae.pilaeTorneo.datos.dao.interfaces.IEquipoDAO;
import com.pilae.pilaeTorneo.datos.dao.interfaces.IEstadoDAO;
import com.pilae.pilaeTorneo.datos.dao.interfaces.IGeneroDAO;
import com.pilae.pilaeTorneo.datos.dao.interfaces.IRolDAO;
import com.pilae.pilaeTorneo.datos.dao.interfaces.ITorneoDAO;
import com.pilae.pilaeTorneo.datos.dao.interfaces.IUsuarioDAO;
import com.pilae.pilaeTorneo.datos.dao.interfaces.IPatrocinadorDAO;
import com.pilae.pilaeTorneo.datos.factoria.concreta.MySQLFactoriaDAO;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import com.pilae.pilaetorneo.utilitarios.sql.enumeracion.FuenteInformacionEnum;
import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;;

public abstract class FactoriaDAO {
	
	public static FactoriaDAO obtenerFactoria(FuenteInformacionEnum fuente) {
		FactoriaDAO retorno;
		
		if(obtenerUtilObjeto().objetoEsNulo(fuente)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de obtener una conexi�n con la fuente de informaci�n";
			final String mensajeTecnico = "Se requiere saber la factoria que se desea obtener";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.DATOS);
		}
		switch (fuente) {
		
		case MY_SQL:
			retorno = new MySQLFactoriaDAO();
			break;

		// will be more in other moment
			
		default:
			final String mensajeUsuario = "Se ha presentado un problema tratando de obtener una conexi�n con la fuente de informaci�n";
			final String mensajeTecnico = "La factoria " + fuente.getNombre() +" no se encuentra implementada.";
			
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.DATOS);
		}
		return retorno;
	}
	
	protected abstract void abrirConexion();
	public abstract void iniciarTransaccion();
	public abstract void confirmarTransaccion();
	public abstract void cancelarTransaccion();
	public abstract void cerrarConexion();

	
	public abstract ICategoriaDAO obtenerCategoriaDAO();
	public abstract IDeporteDAO obtenerDeporteDAO();
	public abstract IEquipoDAO obtenerEquipoDAO();
	public abstract IEstadoDAO obtenerEstadoDAO();
	public abstract IGeneroDAO obtenerGeneroDAO();
	public abstract IRolDAO obtenerRolDAO();
	public abstract ITorneoDAO obtenerTorneoDAO();
	public abstract IUsuarioDAO obtenerUsuarioDAO();
	public abstract IPatrocinadorDAO obtenerPatrocinadorDAO();
}
