package com.pilae.pilaeTorneo.datos.factoria.concreta;

import com.pilae.pilaeTorneo.datos.dao.interfaces.ICategoriaDAO;
import com.pilae.pilaeTorneo.datos.dao.interfaces.IDeporteDAO;
import com.pilae.pilaeTorneo.datos.dao.interfaces.IEquipoDAO;
import com.pilae.pilaeTorneo.datos.dao.interfaces.IEstadoDAO;
import com.pilae.pilaeTorneo.datos.dao.interfaces.IGeneroDAO;
import com.pilae.pilaeTorneo.datos.dao.interfaces.IPatrocinadorDAO;
import com.pilae.pilaeTorneo.datos.dao.interfaces.IRolDAO;
import com.pilae.pilaeTorneo.datos.dao.interfaces.ITorneoDAO;
import com.pilae.pilaeTorneo.datos.dao.interfaces.IUsuarioDAO;
import com.pilae.pilaeTorneo.datos.factoria.abstracta.FactoriaDAO;
import com.pilae.pilaeTorneo.datos.dao.concreta.mysql.CategoriaMySQLDAO;
import com.pilae.pilaeTorneo.datos.dao.concreta.mysql.DeporteMySQLDAO;
import com.pilae.pilaeTorneo.datos.dao.concreta.mysql.EstadoMySQLDAO;
import com.pilae.pilaeTorneo.datos.dao.concreta.mysql.GeneroMySQLDAO;
import com.pilae.pilaeTorneo.datos.dao.concreta.mysql.PatrocinadorMySQLDAO;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;

import static com.pilae.pilaetorneo.utilitarios.sql.UtilSQL.obtenerUtilSQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLFactoriaDAO extends FactoriaDAO{
	
	private Connection conexion;

	public MySQLFactoriaDAO() {		
		super();
		abrirConexion();
	}

	@Override
	protected void abrirConexion() {
		
		if(obtenerUtilSQL().conexionEstaAbierta(this.conexion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de obtener una conexion con la fuente de informacion";
			final String mensajeTecnico = "Se requiere saber la factoria que se desea obtener";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		try {
			final String cadenaConexion = "jdbc:mysql://localhost:3306/db_pilaetorneos?useUnicode=true"
					+ "&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
			
			
			this.conexion = DriverManager.getConnection(cadenaConexion, "root", "admin123");
		} catch (final SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de abrir una conexion con la fuente de informaci�n";
			final String mensajeTecnico = "Se ha presentado un problema tratando de obtener la conexi�n con MySQl. Por favor revise la traza de errores.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.DATOS);
		} catch (Exception excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de abrir una conexi�n con la fuente de informaci�n";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de obtener la conexi�n con MySQL. Por favor revise la traza de errores.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.DATOS); 
		}
	}
	
	@Override
	public void iniciarTransaccion() {
		
		if(!obtenerUtilSQL().conexionEstaAbierta(conexion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de iniciar una operaci�n contra la fuente de informaci�n.";
			final String mensajeTecnico = "Se ha presentado un problema tratando de iniciar una transacci�n contra MySQL. La conexi�n se encuentra cerrada.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.DATOS);
		}
		
		try {
			conexion.setAutoCommit(false);
		} catch (final SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de iniciar una operaci�n contra la fuente de informaci�n.";
			final String mensajeTecnico = "Se ha presentado un problema tratando de iniciar una transacci�n contra MySQL. Por favor revise la traza del error.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.DATOS);
		} catch (final Exception excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de iniciar una operaci�n contra la fuente de informaci�n.";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de iniciar una transacci�n contra MySQL. Por favor revise la traza del error.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.DATOS);
		}
	}

	@Override
	public void confirmarTransaccion() {
		if (!obtenerUtilSQL().conexionEstaAbierta(conexion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de confirmar los cambios de una operaci�n contra la fuente de informaci�n.";
			final String mensajeTecnico = "Se ha presentado un problema tratando de confirmar una transacci�n contra MySQL. La conexi�n se encuentra cerrada.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.DATOS);
		}

		try {
			conexion.commit();
		} catch (final SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de confirmar los cambios de una operaci�n contra la fuente de informaci�n.";
			final String mensajeTecnico = "Se ha presentado un problema tratando de confirmar una transacci�n contra MySQL. Por favor revise la traza del error.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.DATOS);
		} catch (final Exception excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de confirmar los cambios de una operaci�n contra la fuente de informaci�n.";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de confirmar una transacci�n contra MySQl. Por favor revise la traza del error.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.DATOS);
		}
	}

	@Override
	public void cancelarTransaccion() {
		
		if (!obtenerUtilSQL().conexionEstaAbierta(conexion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de cancelar los cambios de una operaci�n contra la fuente de informaci�n.";
			final String mensajeTecnico = "Se ha presentado un problema tratando de cancelar una transacci�n contra MySQ. La conexi�n se encuentra cerrada.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.DATOS);
		}

		try {
			conexion.rollback();
		} catch (final SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de cancelar los cambios de una operaci�n contra la fuente de informaci�n.";
			final String mensajeTecnico = "Se ha presentado un problema tratando de cancelar una transacci�n contra MySQL. Por favor revise la traza del error.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.DATOS);
		} catch (final Exception excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de cancelar los cambios de una operaci�n contra la fuente de informaci�n.";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de cancelar una transacci�n contra MySQL. Por favor revise la traza del error.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.DATOS);
		}
		
	}

	@Override
	public void cerrarConexion() {
		
		if (!obtenerUtilSQL().conexionEstaAbierta(conexion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de cerrar la conexi�n contra la fuente de informaci�n.";
			final String mensajeTecnico = "Se ha presentado un problema tratando de cerrar la conexi�n contra MySQL. La conexi�n ya se encuentra cerrada.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.DATOS);
		}

		try {
			conexion.close();
		} catch (final SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de cerrar la conexi�n contra la fuente de informaci�n.";
			final String mensajeTecnico = "Se ha presentado un problema tratando de cerrar la conexi�n contra MySQL. Por favor revise la traza del error.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.DATOS);
		} catch (final Exception excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de cerrar la conexi�n contra la fuente de informaci�n.";
			final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de cerrar la conexi�n contra MySQL. Por favor revise la traza del error.";
			throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.DATOS);
		}
	}

	@Override
	public ICategoriaDAO obtenerCategoriaDAO() {
		
		if(!obtenerUtilSQL().conexionEstaAbierta(conexion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de realizar la operaci�n deseada en Deporte";
			final String mensajeTecnico = "No es posible crear un CategoriaMySQLDAO con una conexi�n que NO se encuentra abierta";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		return new CategoriaMySQLDAO(conexion);
	}

	@Override
	public IDeporteDAO obtenerDeporteDAO() {
		
		if(!obtenerUtilSQL().conexionEstaAbierta(conexion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de realizar la operaci�n deseada en Deporte";
			final String mensajeTecnico = "No es posible crear un DeporteMySQLDAO con una conexi�n que NO se encuentra abierta";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		return new DeporteMySQLDAO(conexion);
	}

	@Override
	public IEquipoDAO obtenerEquipoDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IEstadoDAO obtenerEstadoDAO() {
		if(!obtenerUtilSQL().conexionEstaAbierta(conexion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de realizar la operaci�n deseada en Estado";
			final String mensajeTecnico = "No es posible crear un EstadoMySQLDAO con una conexi�n que NO se encuentra abierta";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		return new EstadoMySQLDAO(conexion);
	}

	@Override
	public IGeneroDAO obtenerGeneroDAO() {
		
		if(!obtenerUtilSQL().conexionEstaAbierta(conexion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de realizar la operaci�n deseada en Genero";
			final String mensajeTecnico = "No es posible crear un GeneroMySQLDAO con una conexi�n que NO se encuentra abierta";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		return new GeneroMySQLDAO(conexion);
	}

	@Override
	public IRolDAO obtenerRolDAO() {
		// TODO Auto-generated method stub		
		return null;
	}

	@Override
	public ITorneoDAO obtenerTorneoDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IUsuarioDAO obtenerUsuarioDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPatrocinadorDAO obtenerPatrocinadorDAO() {
		
		if(!obtenerUtilSQL().conexionEstaAbierta(conexion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de realizar la operaci�n deseada en Patrocinador";
			final String mensajeTecnico = "No es posible crear un PatrocinadorMySQLDAO con una conexi�n que NO se encuentra abierta";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		return new PatrocinadorMySQLDAO(conexion);
	}

}
