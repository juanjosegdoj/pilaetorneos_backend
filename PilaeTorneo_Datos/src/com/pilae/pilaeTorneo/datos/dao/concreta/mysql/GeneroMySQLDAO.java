package com.pilae.pilaeTorneo.datos.dao.concreta.mysql;

import static com.pilae.pilaetorneo.utilitarios.cadenas.UtilTexto.obtenerUtilTexto;
import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;
import static com.pilae.pilaetorneo.utilitarios.sql.UtilSQL.obtenerUtilSQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.pilae.pilaeTorneo.datos.dao.interfaces.IGeneroDAO;
import com.pilae.pilaetorneo.dominio.GeneroDominio;
import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;

public class GeneroMySQLDAO implements IGeneroDAO{

	private final Connection conexion;
	
	public GeneroMySQLDAO(final Connection conexion) {
		super();
		
		if(!obtenerUtilSQL().conexionEstaAbierta(conexion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de llevar acabo la operación deseada sobre un genero";
			final String mensajeTecnico = "No es posible crear un GeneroMySQLDAO con una conexión que NO se encuentra abierta";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		this.conexion = conexion;
	}

	@Override
	public void crear(final GeneroDominio genero) {
		if(!OperacionEnum.CREAR.equals(genero.getOperacion())) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de registrar la información del nuevo genero, en la fuente de información deseada";
			final String mensajeTecnico = "No es posible crear un objeto Genero cuya operación no corresponde a CREAR";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		final StringBuilder sentenciaBuilder = new StringBuilder();
		sentenciaBuilder.append("insert into genero_tbl(nombre)");
		sentenciaBuilder.append("values (?)");
		
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			// parámetros de la sentencia sql
			sentenciaPreparada.setString(1, genero.getNombre());
			
			sentenciaPreparada.executeUpdate();
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de registrar el genero en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de crear el Genero en la base de datos MySQL";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
	}

	@Override
	public void actualizar(final GeneroDominio genero) {
		
		if(!OperacionEnum.ACTUALIZAR.equals(genero.getOperacion())) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de modificar la información del nuevo Genero, en la fuente de información deseada";
			final String mensajeTecnico = "No es posible actualizar un objeto Genero cuya operación no corresponde a ACTUALIZAR";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		final StringBuilder sentenciaBuilder = new StringBuilder();
		
		sentenciaBuilder.append("UPDATE genero_tbl ");
		sentenciaBuilder.append("SET nombre = ? ");
		sentenciaBuilder.append("WHERE id_genero = ?");
		
		
		
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			// Colocar parámetros a la sentencia sql
			sentenciaPreparada.setString(1, genero.getNombre());
			sentenciaPreparada.setInt(2, genero.getIdGenero());
			
			System.out.println(sentenciaBuilder.toString());
			sentenciaPreparada.executeUpdate();
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de modificar la información del genero en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de actualizar el Genero en la base de datos MySQL";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
	}

	@Override
	public void eliminar(final GeneroDominio genero) {
		
		if(!OperacionEnum.ELIMINAR.equals(genero.getOperacion())) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de dar de baja a la información del nuevo genero, en la fuente de información deseada";
			final String mensajeTecnico = "No es posible eliminar un objeto Genero cuya operación no corresponde a ELIMINAR";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		final StringBuilder sentenciaBuilder = new StringBuilder();
		
		sentenciaBuilder.append(" DELETE ");
		sentenciaBuilder.append(" FROM genero_tbl ");
		sentenciaBuilder.append(" WHERE id_genero = ? ");
		
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			// Colocar parámetros a la sentencia sql
			sentenciaPreparada.setInt(1, genero.getIdGenero());
			
			sentenciaPreparada.executeUpdate();
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de bajar físicamente la información del genero en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de eliminar el Genero en la base de datos MySQL";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
	}

	@Override
	public List<GeneroDominio> consultar(final GeneroDominio genero) {
		
		final List<Object> listaParametros = new ArrayList<>();
		final List<GeneroDominio> listaResultados = new ArrayList<>();
		boolean colocarWhere = true;
		final StringBuilder sentenciaBuilder = new StringBuilder();
		
		sentenciaBuilder.append("SELECT id_genero, nombre");
		sentenciaBuilder.append(" FROM genero_tbl ");
				
		
		if(!obtenerUtilObjeto().objetoEsNulo(genero)) {
						
			if(OperacionEnum.CONSULTAR != genero.getOperacion()) {
				final String mensajeUsuario = "Se ha presentado un problema tratando de consultar la información de los generos, en la fuente de información deseada";
				final String mensajeTecnico = "No es posible consultar objetos del tipo objeto Genero cuya operación no corresponde a CONSULTAR";

				throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
			}
			
			
			if(genero.getIdGenero() > 0) {
				sentenciaBuilder.append("WHERE id_genero = ? ");
				listaParametros.add(genero.getIdGenero());
				colocarWhere = false;
			}
			if(!obtenerUtilTexto().cadenaEsVaciaONula(genero.getNombre())) {
				if(colocarWhere) {
					sentenciaBuilder.append("WHERE ");
				}else {
					sentenciaBuilder.append("AND   ");
				}
				sentenciaBuilder.append("nombre LIKE ? ");
				listaParametros.add(genero.getNombre());
				colocarWhere = false;
			}
		}
		
		sentenciaBuilder.append("ORDER BY nombre");  // Poner al final
				
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			for (int indice = 0; indice < listaParametros.size(); indice++) {
				sentenciaPreparada.setObject(indice + 1, listaParametros.get(indice));
			}
			
			
			try(final ResultSet cursorResultados = sentenciaPreparada.executeQuery()){
				while(cursorResultados.next()) {
					listaResultados.add(
							GeneroDominio.crear(
									cursorResultados.getInt("id_genero"), 
									cursorResultados.getString("nombre"),  
									OperacionEnum.POBLAR)
							);
				}
			}
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de consultar la información de los generos en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de consultar los Generos en la base de datos MySQL.";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
		return listaResultados;
	}

}
