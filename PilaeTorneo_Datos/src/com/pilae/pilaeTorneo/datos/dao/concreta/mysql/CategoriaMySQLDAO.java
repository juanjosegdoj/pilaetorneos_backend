package com.pilae.pilaeTorneo.datos.dao.concreta.mysql;

import static com.pilae.pilaetorneo.utilitarios.fecha.UtilFecha.obtenerUtilFecha;
import static com.pilae.pilaetorneo.utilitarios.sql.UtilSQL.obtenerUtilSQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.pilae.pilaeTorneo.datos.dao.interfaces.ICategoriaDAO;
import com.pilae.pilaetorneo.dominio.CategoriaDominio;
import com.pilae.pilaetorneo.dominio.DeporteDominio;
import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import com.pilae.pilaetorneo.utilitarios.fecha.UtilFecha;

import static com.pilae.pilaetorneo.utilitarios.cadenas.UtilTexto.obtenerUtilTexto;
import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;
public final class CategoriaMySQLDAO implements ICategoriaDAO{

	private final Connection conexion;
	
	public CategoriaMySQLDAO(final Connection conexion) {
		
		if(!obtenerUtilSQL().conexionEstaAbierta(conexion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de llevar acabo la operación deseada sobre una Categoria.";
			final String mensajeTecnico = "No es posible crear un CategoriaMySQLDAO con una conexión que NO se encuentra abierta";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		this.conexion = conexion;
	}
	
	@Override
	public void crear(CategoriaDominio categoria) {
		
		if(!OperacionEnum.CREAR.equals(categoria.getOperacion())) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de registrar la información de la nueva Categoria, en la fuente de información deseada";
			final String mensajeTecnico = "No es posible crear un objeto Categoria cuya operación no corresponde a CREAR";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		final StringBuilder sentenciaBuilder = new StringBuilder();
		sentenciaBuilder.append("insert into categoria_tbl(nombre, fecha_nacim_maxima, fecha_nacim_minima, id_deporte, anio) ");
		sentenciaBuilder.append("values (?, ?, ?, ?, ?)"); 
		
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			// Colocar parámetros a la sentencia sql
			sentenciaPreparada.setString(1, categoria.getNombre());
			sentenciaPreparada.setString(2, obtenerUtilFecha().dateAString(categoria.getFechaNacimientoMaxima()));
			sentenciaPreparada.setString(3, obtenerUtilFecha().dateAString(categoria.getFechaNacimientoMinimo()));
			sentenciaPreparada.setInt(4, categoria.getDeporte().getIdDeporte());
			sentenciaPreparada.setInt(5, categoria.getAnio());
			
			sentenciaPreparada.executeUpdate();
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de registrar la Categoria en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de crear la Categoria en la base de datos MySQL";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
	}

	@Override
	public void actualizar(CategoriaDominio categoria) {
		
		if(!OperacionEnum.ACTUALIZAR.equals(categoria.getOperacion())) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de modificar la información de la categoría, en la fuente de información deseada";
			final String mensajeTecnico = "No es posible actualizar un objeto Categoria cuya operación no corresponde a ACTUALIZAR";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		final StringBuilder sentenciaBuilder = new StringBuilder();
		
		sentenciaBuilder.append("UPDATE categoria_tbl ");
		sentenciaBuilder.append("SET nombre = ?,");
		sentenciaBuilder.append("    fecha_nacim_maxima = ?,");
		sentenciaBuilder.append("    fecha_nacim_minima = ?,");
		sentenciaBuilder.append("    id_deporte = ?,");
		sentenciaBuilder.append("    anio = ?");
		sentenciaBuilder.append("WHERE id_categoria = ?");
		
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			// Colocar parámetros a la sentencia sql
			sentenciaPreparada.setString(1, categoria.getNombre());
			sentenciaPreparada.setString(2, obtenerUtilFecha().dateAString(categoria.getFechaNacimientoMaxima()));
			sentenciaPreparada.setString(3, obtenerUtilFecha().dateAString(categoria.getFechaNacimientoMinimo()));
			sentenciaPreparada.setInt(4, categoria.getDeporte().getIdDeporte());
			sentenciaPreparada.setInt(5, categoria.getAnio());
			sentenciaPreparada.setInt(6, categoria.getIdCategoria());
			
			sentenciaPreparada.executeUpdate();
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de modificar la información de la Categoria en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de actualizar la Categoria en la base de datos MySQL";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
		
	}

	@Override
	public void eliminar(CategoriaDominio categoria) {
		
		if(!OperacionEnum.ELIMINAR.equals(categoria.getOperacion())) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de dar de baja a la información de la Categoria, en la fuente de información deseada";
			final String mensajeTecnico = "No es posible eliminar un objeto Categoria cuya operación no corresponde a ELIMINAR";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		final StringBuilder sentenciaBuilder = new StringBuilder();
		
		sentenciaBuilder.append(" DELETE ");
		sentenciaBuilder.append(" FROM categoria_tbl ");
		sentenciaBuilder.append(" WHERE id_categoria = ? ");
		
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			// Colocar parámetros a la sentencia sql
			sentenciaPreparada.setInt(1, categoria.getDeporte().getIdDeporte());
			
			sentenciaPreparada.executeUpdate();
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de bajar físicamente la información de la categoria en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de eliminar la categoria en la base de datos MySQL";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
		
	}

	@Override
	public List<CategoriaDominio> consultar(CategoriaDominio categoria) {
		
		
		
		final List<Object> listaParametros = new ArrayList<>();
		final List<CategoriaDominio> listaResultados = new ArrayList<>();
		boolean colocarWhere = true;
		final StringBuilder sentenciaBuilder = new StringBuilder();
		
		sentenciaBuilder.append("SELECT cat.id_categoria, cat.nombre, cat.fecha_nacim_maxima, cat.fecha_nacim_minima, cat.anio, ");
		sentenciaBuilder.append("       dep.id_deporte, dep.nombre, dep.descripcion ");
		sentenciaBuilder.append(" FROM deporte_tbl AS dep ");
		sentenciaBuilder.append(" INNER JOIN categoria_tbl cat ON (cat.id_deporte = dep.id_deporte) ");
		
		if(categoria.getIdCategoria() > 0) {
			sentenciaBuilder.append("WHERE cat.id_categoria = ? ");
			listaParametros.add(categoria.getIdCategoria());
			colocarWhere = false;
		}
		if(!obtenerUtilTexto().cadenaEsVaciaONula(categoria.getNombre())) {
			if(colocarWhere) {
				sentenciaBuilder.append("WHERE ");
			}else {
				sentenciaBuilder.append("AND ");
			}
			sentenciaBuilder.append("cat.nombre like ? ");
			listaParametros.add(categoria.getNombre());
			colocarWhere = false;
		}
		if(categoria.getAnio() > 0) {
			if(colocarWhere) {
				sentenciaBuilder.append("WHERE ");
			}else {
				sentenciaBuilder.append("AND ");
			}
			sentenciaBuilder.append("cat.anio like ? ");
			listaParametros.add(categoria.getAnio());
			colocarWhere = false;
		}
		if(!obtenerUtilObjeto().objetoEsNulo(categoria.getDeporte()) && categoria.getDeporte().getIdDeporte() > 0) {
			if(colocarWhere) {
				sentenciaBuilder.append("WHERE ");
			}else {
				sentenciaBuilder.append("AND ");
			}
			sentenciaBuilder.append("dep.id_deporte = ? ");
			listaParametros.add(categoria.getDeporte().getIdDeporte());
			colocarWhere = false;
		}
		
		if(!obtenerUtilObjeto().objetoEsNulo(categoria.getDeporte()) && !obtenerUtilTexto().cadenaEsVaciaONula(categoria.getDeporte().getNombre())) {
			if(colocarWhere) {
				sentenciaBuilder.append("WHERE ");
			}else {
				sentenciaBuilder.append("AND ");
			}
			sentenciaBuilder.append("dep.nombre like ? ");
			listaParametros.add(categoria.getDeporte().getNombre());
			colocarWhere = false;
		}
		sentenciaBuilder.append("ORDER BY cat.nombre");
		
		
		
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			for (int indice = 0; indice < listaParametros.size(); indice++) {
				sentenciaPreparada.setObject(indice+1, listaParametros.get(indice));
			}
			
			try (final ResultSet cursorResultados = sentenciaPreparada.executeQuery()) {
				
				while(cursorResultados.next()) {
					
					listaResultados.add(
							CategoriaDominio.crear(			
									cursorResultados.getInt("cat.id_categoria"), 
									cursorResultados.getString("cat.nombre"),
									UtilFecha.obtenerUtilFecha().stringADate(cursorResultados.getString("cat.fecha_nacim_maxima")),
									UtilFecha.obtenerUtilFecha().stringADate(cursorResultados.getString("cat.fecha_nacim_minima")),
									
									DeporteDominio.crear(
											cursorResultados.getInt("dep.id_deporte"), 
											cursorResultados.getString("dep.nombre"), 
											cursorResultados.getString("dep.descripcion"), 
											OperacionEnum.POBLAR), 
									cursorResultados.getInt("cat.anio"), 
									OperacionEnum.POBLAR)
							);
				}
				System.out.println("Voy muy lejos en dao despues");
			} catch (ParseException excepcion) {
				String mensajeTecnico = "Se ha presentado un problema al parsear la fechaNacimientoMaxima y/o fechaNacimientoMinimo";
				String mensajeUsuario = "Se ha presentado un problema tratando de consultar las Categorias en la base de datos MySQL.";
				throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.DATOS);
			}
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de consultar la información de las categorias en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de consultar las Categorias en la base de datos MySQL.";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
		
		return listaResultados;
	}

}
