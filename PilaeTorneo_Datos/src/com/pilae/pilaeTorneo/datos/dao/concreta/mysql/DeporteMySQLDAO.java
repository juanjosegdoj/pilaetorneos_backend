package com.pilae.pilaeTorneo.datos.dao.concreta.mysql;

import static com.pilae.pilaetorneo.utilitarios.sql.UtilSQL.obtenerUtilSQL;
import static com.pilae.pilaetorneo.utilitarios.cadenas.UtilTexto.obtenerUtilTexto;
import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.pilae.pilaeTorneo.datos.dao.interfaces.IDeporteDAO;
import com.pilae.pilaetorneo.dominio.DeporteDominio;
import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;


public final class DeporteMySQLDAO implements IDeporteDAO{
	
	private final Connection conexion;
	
	public DeporteMySQLDAO(final Connection conexion) {
		
		if(!obtenerUtilSQL().conexionEstaAbierta(conexion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de llevar acabo la operación deseada sobre un deporte";
			final String mensajeTecnico = "No es posible crear un DeporteMySQLDAO con una conexión que NO se encuentra abierta";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		this.conexion = conexion;
	}
	
	@Override
	public void crear(final DeporteDominio deporte) {
		if(!OperacionEnum.CREAR.equals(deporte.getOperacion())) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de registrar la información del nuevo deporte, en la fuente de información deseada";
			final String mensajeTecnico = "No es posible crear un objeto Deporte cuya operación no corresponde a CREAR";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		final StringBuilder sentenciaBuilder = new StringBuilder();
		sentenciaBuilder.append("insert into deporte_tbl(nombre, descripcion)");
		sentenciaBuilder.append("values (?, ?)");
		
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			// Colocar parámetros a la sentencia sql
			sentenciaPreparada.setString(1, deporte.getNombre());
			sentenciaPreparada.setString(2, deporte.getDescripcion());
			
			sentenciaPreparada.executeUpdate();
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de registrar el deporte en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de crear el Deporte en la base de datos MySQL";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
	}

	@Override
	public void actualizar(final DeporteDominio deporte) {
		
		if(!OperacionEnum.ACTUALIZAR.equals(deporte.getOperacion())) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de modificar la información del nuevo deporte, en la fuente de información deseada";
			final String mensajeTecnico = "No es posible actualizar un objeto Deporte cuya operación no corresponde a ACTUALIZAR";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		final StringBuilder sentenciaBuilder = new StringBuilder();
		
		sentenciaBuilder.append("UPDATE deporte_tbl ");
		sentenciaBuilder.append("SET nombre = ?,");
		sentenciaBuilder.append("    descripcion = ?");
		sentenciaBuilder.append("WHERE id_deporte = ?");
		
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			// Colocar parámetros a la sentencia sql
			sentenciaPreparada.setString(1, deporte.getNombre());
			sentenciaPreparada.setString(2, deporte.getDescripcion());
			sentenciaPreparada.setInt(3, deporte.getIdDeporte());
			
			sentenciaPreparada.executeUpdate();
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de modificar la información del deporte en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de actualizar el Deporte en la base de datos MySQL";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
	}

	@Override
	public void eliminar(final DeporteDominio deporte) {
		
		if(!OperacionEnum.ELIMINAR.equals(deporte.getOperacion())) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de dar de baja a la información del nuevo deporte, en la fuente de información deseada";
			final String mensajeTecnico = "No es posible eliminar un objeto Deporte cuya operación no corresponde a ELIMINAR";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		final StringBuilder sentenciaBuilder = new StringBuilder();
		
		sentenciaBuilder.append(" DELETE ");
		sentenciaBuilder.append(" FROM deporte_tbl ");
		sentenciaBuilder.append(" WHERE id_deporte = ? ");
		
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			// Colocar parámetros a la sentencia sql
			sentenciaPreparada.setInt(1, deporte.getIdDeporte());
			
			sentenciaPreparada.executeUpdate();
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de bajar físicamente la información del deporte en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de eliminar el Deporte en la base de datos MySQL";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
	}

	@Override
	public List<DeporteDominio> consultar(final DeporteDominio deporte) {
		
		final List<Object> listaParametros = new ArrayList<>();
		final List<DeporteDominio> listaResultados = new ArrayList<>();
		boolean colocarWhere = true;
		final StringBuilder sentenciaBuilder = new StringBuilder();
		
		sentenciaBuilder.append("SELECT id_deporte, nombre, descripcion");
		sentenciaBuilder.append(" FROM deporte_tbl ");
				
		
		if(!obtenerUtilObjeto().objetoEsNulo(deporte)) {
						
			if(OperacionEnum.CONSULTAR != deporte.getOperacion()) {
				final String mensajeUsuario = "Se ha presentado un problema tratando de consultar la información de los deportes, en la fuente de información deseada";
				final String mensajeTecnico = "No es posible consultar objetos del tipo objeto Deporte cuya operación no corresponde a CONSULTAR";

				throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
			}
			
			
			if(deporte.getIdDeporte() > 0) {
				sentenciaBuilder.append("WHERE id_deporte = ? ");
				listaParametros.add(deporte.getIdDeporte());
				colocarWhere = false;
			}
			if(!obtenerUtilTexto().cadenaEsVaciaONula(deporte.getNombre())) {
				if(colocarWhere) {
					sentenciaBuilder.append("WHERE ");
				}else {
					sentenciaBuilder.append("AND   ");
				}
				sentenciaBuilder.append("nombre LIKE ? ");
				listaParametros.add(deporte.getNombre());
				colocarWhere = false;
			}
		}
		
		sentenciaBuilder.append("ORDER BY nombre"); 
				
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			for (int indice = 0; indice < listaParametros.size(); indice++) {
				sentenciaPreparada.setObject(indice + 1, listaParametros.get(indice));
			}
			
			
			try(final ResultSet cursorResultados = sentenciaPreparada.executeQuery()){
				while(cursorResultados.next()) {
					
					listaResultados.add(
							DeporteDominio.crear(
									cursorResultados.getInt("id_deporte"), 
									cursorResultados.getString("nombre"),  
									cursorResultados.getString("descripcion"), 
									OperacionEnum.POBLAR)
							);
				}
			}
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de consultar la información de los deportes en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de consultar los Deportes en la base de datos MySQL.";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
		return listaResultados;
	}

}
