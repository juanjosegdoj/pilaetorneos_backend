package com.pilae.pilaeTorneo.datos.dao.concreta.mysql;

import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;
import static com.pilae.pilaetorneo.utilitarios.sql.UtilSQL.obtenerUtilSQL;
import static com.pilae.pilaetorneo.utilitarios.cadenas.UtilTexto.obtenerUtilTexto;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.pilae.pilaeTorneo.datos.dao.interfaces.IEstadoDAO;
import com.pilae.pilaetorneo.dominio.EstadoDominio;
import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;;


public class EstadoMySQLDAO implements IEstadoDAO {
	
	private final Connection conexion;
	
	public EstadoMySQLDAO(final Connection conexion) {
		
		if(!obtenerUtilSQL().conexionEstaAbierta(conexion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de llevar acabo la operación deseada sobre un Estado";
			final String mensajeTecnico = "No es posible crear un EstadoMySQLDAO con una conexión que NO se encuentra abierta";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		this.conexion = conexion;
	}
	
	@Override
	public void crear(final EstadoDominio estado) {
		if(!OperacionEnum.CREAR.equals(estado.getOperacion())) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de registrar la información del nuevo Estado, en la fuente de información deseada";
			final String mensajeTecnico = "No es posible crear un objeto Estado cuya operación no corresponde a CREAR";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		final StringBuilder sentenciaBuilder = new StringBuilder();
		sentenciaBuilder.append(" insert into estado_tbl(nombre, descripcion) ");
		sentenciaBuilder.append(" values (?, ?) ");
		
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			// Colocar parámetros a la sentencia sql
			sentenciaPreparada.setString(1, estado.getNombre());
			sentenciaPreparada.setString(2, estado.getDescripcion());
			
			sentenciaPreparada.executeUpdate();
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de registrar el Estado en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de crear el objeto Estado en la base de datos MySQL";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
	}

	@Override
	public void actualizar(final EstadoDominio estado) {
		
		if(!OperacionEnum.ACTUALIZAR.equals(estado.getOperacion())) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de modificar la información del nuevo Estado, en la fuente de información deseada";
			final String mensajeTecnico = "No es posible actualizar un objeto Estado cuya operación no corresponde a ACTUALIZAR";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		final StringBuilder sentenciaBuilder = new StringBuilder();
		
		sentenciaBuilder.append(" UPDATE estado_tbl ");
		sentenciaBuilder.append(" SET nombre = ?, ");
		sentenciaBuilder.append("    descripcion = ? ");
		sentenciaBuilder.append(" WHERE id_estado = ? ");
		
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			// Colocar parámetros a la sentencia sql
			sentenciaPreparada.setString(1, estado.getNombre());
			sentenciaPreparada.setString(2, estado.getDescripcion());
			sentenciaPreparada.setInt(3, estado.getIdEstado());
			
			sentenciaPreparada.executeUpdate();
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de modificar la información del Estado en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de actualizar el Estado en la base de datos MySQL";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
	}

	@Override
	public void eliminar(final EstadoDominio estado) {
		
		if(!OperacionEnum.ELIMINAR.equals(estado.getOperacion())) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de dar de baja a la información del Estado, en la fuente de información deseada";
			final String mensajeTecnico = "No es posible eliminar un objeto Estado cuya operación no corresponde a ELIMINAR";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		final StringBuilder sentenciaBuilder = new StringBuilder();
		
		sentenciaBuilder.append(" DELETE ");
		sentenciaBuilder.append(" FROM estado_tbl ");
		sentenciaBuilder.append(" WHERE id_estado = ? ");
		
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			// Colocar parámetros a la sentencia sql
			sentenciaPreparada.setInt(1, estado.getIdEstado());
			
			sentenciaPreparada.executeUpdate();
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de bajar físicamente la información del Estado en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de eliminar el Estado en la base de datos MySQL";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
	}

	@Override
	public List<EstadoDominio> consultar(final EstadoDominio estado) {
		
		
		final List<Object> listaParametros = new ArrayList<>();
		final List<EstadoDominio> listaResultados = new ArrayList<>();
		boolean colocarWhere = true;
		final StringBuilder sentenciaBuilder = new StringBuilder();
		
		sentenciaBuilder.append("SELECT id_estado, nombre, descripcion");
		sentenciaBuilder.append(" FROM estado_tbl ");
				
		
		if(!obtenerUtilObjeto().objetoEsNulo(estado)) {
						
			if(OperacionEnum.CONSULTAR != estado.getOperacion()) {
				final String mensajeUsuario = "Se ha presentado un problema tratando de consultar la información de los estados, en la fuente de información deseada";
				final String mensajeTecnico = "No es posible consultar objetos del tipo objeto Estado cuya operación no corresponde a CONSULTAR";

				throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
			}
			
			
			if(estado.getIdEstado() > 0) {
				sentenciaBuilder.append(" WHERE id_estado = ? ");
				listaParametros.add(estado.getIdEstado());
				colocarWhere = false;
			}
			if(!obtenerUtilTexto().cadenaEsVaciaONula(estado.getNombre())) {
				if(colocarWhere) {
					sentenciaBuilder.append("WHERE ");
				}else {
					sentenciaBuilder.append("AND   ");
				}
				sentenciaBuilder.append("nombre LIKE ? ");
				listaParametros.add(estado.getNombre());
				colocarWhere = false;
			}
		}
		
		sentenciaBuilder.append("ORDER BY nombre");  // Poner al final
				
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			
			for (int indice = 0; indice < listaParametros.size(); indice++) {
				sentenciaPreparada.setObject(indice + 1, listaParametros.get(indice));
			}
			
			
			try(final ResultSet cursorResultados = sentenciaPreparada.executeQuery()){
				while(cursorResultados.next()) {
					listaResultados.add(
							EstadoDominio.crear(
									cursorResultados.getInt("id_estado"), 
									cursorResultados.getString("nombre"),  
									cursorResultados.getString("descripcion"), 
									OperacionEnum.POBLAR)
							);
				}
			}
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de consultar la información de los estados en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de consultar los Estados en la base de datos MySQL.";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
		return listaResultados;
	}
}
