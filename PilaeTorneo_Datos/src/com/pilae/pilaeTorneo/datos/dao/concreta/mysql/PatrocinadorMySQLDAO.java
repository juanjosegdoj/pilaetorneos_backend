package com.pilae.pilaeTorneo.datos.dao.concreta.mysql;

import static com.pilae.pilaetorneo.utilitarios.cadenas.UtilTexto.obtenerUtilTexto;
import static com.pilae.pilaetorneo.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;
import static com.pilae.pilaetorneo.utilitarios.sql.UtilSQL.obtenerUtilSQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.pilae.pilaeTorneo.datos.dao.interfaces.IPatrocinadorDAO;
import com.pilae.pilaetorneo.dominio.PatrocinadorDominio;
import com.pilae.pilaetorneo.utilitarios.dominio.enumeracion.OperacionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import com.pilae.pilaetorneo.utilitarios.excepcion.excepcion.AplicacionExcepcion;

public class PatrocinadorMySQLDAO implements IPatrocinadorDAO {
	private final Connection conexion;
	
	public PatrocinadorMySQLDAO(final Connection conexion) {
		if(!obtenerUtilSQL().conexionEstaAbierta(conexion)) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de llevar acabo la operación deseada sobre un patrocinador";
			final String mensajeTecnico = "No es posible crear un PatrocinadorMySQLDAO con una conexión que NO se encuentra abierta";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		this.conexion = conexion;
	}

	@Override
	public void crear(PatrocinadorDominio patrocinador) {
		
		if(!OperacionEnum.CREAR.equals(patrocinador.getOperacion())) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de registrar la información del nuevo patrocinador, en la fuente de información deseada";
			final String mensajeTecnico = "No es posible crear un objeto Patrocinador cuya operación no corresponde a CREAR";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		final StringBuilder sentenciaBuilder = new StringBuilder();
		sentenciaBuilder.append("insert into patrocinador_tbl(nombre, descripcion)");
		sentenciaBuilder.append("values (?, ?)");
		
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			// Colocar parámetros a la sentencia sql
			sentenciaPreparada.setString(1, patrocinador.getNombre());
			sentenciaPreparada.setString(2, patrocinador.getDescripcion());
			
			sentenciaPreparada.executeUpdate();
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de registrar el patrocinador en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de crear el Patrocinador en la base de datos MySQL";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
	}

	@Override
	public void actualizar(PatrocinadorDominio patrocinador) {
		if(!OperacionEnum.ACTUALIZAR.equals(patrocinador.getOperacion())) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de modificar la información del nuevo patrocinador, en la fuente de información deseada";
			final String mensajeTecnico = "No es posible actualizar un objeto patrocinador cuya operación no corresponde a ACTUALIZAR";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		final StringBuilder sentenciaBuilder = new StringBuilder();
		
		sentenciaBuilder.append("UPDATE patrocinador_tbl ");
		sentenciaBuilder.append("SET nombre = ?,");
		sentenciaBuilder.append("    descripcion = ?");
		sentenciaBuilder.append("WHERE id_patrocinador = ?");
		
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			// Colocar parámetros a la sentencia sql
			sentenciaPreparada.setString(1, patrocinador.getNombre());
			sentenciaPreparada.setString(2, patrocinador.getDescripcion());
			sentenciaPreparada.setInt(3, patrocinador.getIdPatrocinador());
			
			sentenciaPreparada.executeUpdate();
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de modificar la información del patrocinador en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de actualizar el patrocinador en la base de datos MySQL";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
	}

	@Override
	public void eliminar(PatrocinadorDominio patrocinador) {
		if(!OperacionEnum.ELIMINAR.equals(patrocinador.getOperacion())) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de dar de baja a la información del nuevo patrocinador, en la fuente de información deseada";
			final String mensajeTecnico = "No es posible eliminar un objeto patrocinador cuya operación no corresponde a ELIMINAR";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
		}
		
		final StringBuilder sentenciaBuilder = new StringBuilder();
		
		sentenciaBuilder.append(" DELETE ");
		sentenciaBuilder.append(" FROM patrocinador_tbl ");
		sentenciaBuilder.append(" WHERE id_patrocinador = ? ");
		
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			// Colocar parámetros a la sentencia sql
			sentenciaPreparada.setInt(1, patrocinador.getIdPatrocinador());
			
			sentenciaPreparada.executeUpdate();
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de bajar físicamente la información del patrocinador en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de eliminar el patrocinador en la base de datos MySQL";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
		
	}

	@Override
	public List<PatrocinadorDominio> consultar(PatrocinadorDominio patrocinador) {
		final List<Object> listaParametros = new ArrayList<>();
		final List<PatrocinadorDominio> listaResultados = new ArrayList<>();
		boolean colocarWhere = true;
		final StringBuilder sentenciaBuilder = new StringBuilder();
		
		sentenciaBuilder.append("SELECT id_patrocinador, nombre, descripcion");
		sentenciaBuilder.append(" FROM patrocinador_tbl ");
				
		
		if(!obtenerUtilObjeto().objetoEsNulo(patrocinador)) {
						
			if(OperacionEnum.CONSULTAR != patrocinador.getOperacion()) {
				final String mensajeUsuario = "Se ha presentado un problema tratando de consultar la información de los patrocinadores, en la fuente de información deseada";
				final String mensajeTecnico = "No es posible consultar objetos del tipo objeto patrocinador cuya operación no corresponde a CONSULTAR";

				throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico , ExcepcionEnum.DATOS);
			}
			
			
			if(patrocinador.getIdPatrocinador() > 0) {
				sentenciaBuilder.append("WHERE id_patrocinador = ? ");
				listaParametros.add(patrocinador.getIdPatrocinador());
				colocarWhere = false;
			}
			if(!obtenerUtilTexto().cadenaEsVaciaONula(patrocinador.getNombre())) {
				if(colocarWhere) {
					sentenciaBuilder.append("WHERE ");
				}else {
					sentenciaBuilder.append("AND   ");
				}
				sentenciaBuilder.append("nombre LIKE ? ");
				listaParametros.add(patrocinador.getNombre());
				colocarWhere = false;
			}
		}
		
		sentenciaBuilder.append("ORDER BY nombre");  // Poner al final
				
		try(final PreparedStatement sentenciaPreparada = conexion.prepareStatement(sentenciaBuilder.toString())){
			
			for (int indice = 0; indice < listaParametros.size(); indice++) {
				sentenciaPreparada.setObject(indice + 1, listaParametros.get(indice));
			}
			
			
			try(final ResultSet cursorResultados = sentenciaPreparada.executeQuery()){
				while(cursorResultados.next()) {
					listaResultados.add(
							PatrocinadorDominio.crear(
									cursorResultados.getInt("id_patrocinador"), 
									cursorResultados.getString("nombre"),  
									cursorResultados.getString("descripcion"), 
									OperacionEnum.POBLAR)
							);
				}
			}
			
		} catch (SQLException excepcion) {
			final String mensajeUsuario = "Se ha presentado un problema tratando de consultar la información de los patrocinadores en la fuente de información deseada";
			final String mensajeTecnico = "Se ha presentado un problema tratando de consultar los patrocinadores en la base de datos MySQL.";
			
			throw AplicacionExcepcion.CREAR(mensajeUsuario, mensajeTecnico, excepcion, ExcepcionEnum.DATOS);
		}
		return listaResultados;
	}
}
