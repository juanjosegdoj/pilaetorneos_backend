package com.pilae.pilaeTorneo.datos.dao.interfaces;

import java.util.List;

import com.pilae.pilaetorneo.dominio.EquipoDominio;;

public interface IEquipoDAO {
	void crear(EquipoDominio equipo);
	void actualizar(EquipoDominio equipo);
	void eliminar(EquipoDominio equipo);
	List<EquipoDominio> consultar(EquipoDominio equipo);

}
