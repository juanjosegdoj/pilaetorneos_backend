package com.pilae.pilaeTorneo.datos.dao.interfaces;

import java.util.List;

import com.pilae.pilaetorneo.dominio.CategoriaDominio;;

public interface ICategoriaDAO {
	void crear(CategoriaDominio categoria);
	void actualizar(CategoriaDominio categoria);
	void eliminar(CategoriaDominio categoria);
	List<CategoriaDominio> consultar(CategoriaDominio categoria);

}
