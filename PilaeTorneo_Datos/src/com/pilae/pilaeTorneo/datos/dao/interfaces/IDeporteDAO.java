package com.pilae.pilaeTorneo.datos.dao.interfaces;

import java.util.List;

import com.pilae.pilaetorneo.dominio.DeporteDominio;

public interface IDeporteDAO {
	void crear(DeporteDominio deporte);
	void actualizar(DeporteDominio deporte);
	void eliminar(DeporteDominio deporte);
	List<DeporteDominio> consultar(DeporteDominio deporte);
}
