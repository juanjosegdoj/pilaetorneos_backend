package com.pilae.pilaeTorneo.datos.dao.interfaces;

import java.util.List;

import com.pilae.pilaetorneo.dominio.GeneroDominio;

public interface IGeneroDAO {
	void crear(GeneroDominio genero);
	void actualizar(GeneroDominio genero);
	void eliminar(GeneroDominio genero);
	List<GeneroDominio> consultar(GeneroDominio genero);
}
