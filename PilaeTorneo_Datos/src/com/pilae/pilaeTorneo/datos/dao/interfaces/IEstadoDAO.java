package com.pilae.pilaeTorneo.datos.dao.interfaces;

import java.util.List;

import com.pilae.pilaetorneo.dominio.EstadoDominio;;

public interface IEstadoDAO {
	void crear(EstadoDominio estado);
	void actualizar(EstadoDominio estado);
	void eliminar(EstadoDominio estado);
	List<EstadoDominio> consultar(EstadoDominio estado);
}
