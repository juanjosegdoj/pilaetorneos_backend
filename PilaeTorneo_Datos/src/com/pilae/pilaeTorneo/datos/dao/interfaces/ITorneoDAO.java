package com.pilae.pilaeTorneo.datos.dao.interfaces;

import java.util.List;

import com.pilae.pilaetorneo.dominio.TorneoDominio;

public interface ITorneoDAO {
	
	void crear(TorneoDominio torneo);
	void actualizar(TorneoDominio torneo);
	void eliminar(TorneoDominio torneo);
	List<TorneoDominio> consultar(TorneoDominio torneo);

}
