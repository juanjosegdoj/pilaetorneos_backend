package com.pilae.pilaeTorneo.datos.dao.interfaces;

import java.util.List;

import com.pilae.pilaetorneo.dominio.RolDominio;;

public interface IRolDAO {
	
	void crear(RolDominio rol);
	void actualizar(RolDominio rol);
	void eliminar(RolDominio rol);
	List<RolDominio> consultar(RolDominio rol);
	
}
