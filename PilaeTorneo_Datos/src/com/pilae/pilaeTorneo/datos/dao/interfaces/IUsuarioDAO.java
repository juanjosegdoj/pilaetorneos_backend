package com.pilae.pilaeTorneo.datos.dao.interfaces;

import java.util.List;

import com.pilae.pilaetorneo.dominio.UsuarioDominio;

public interface IUsuarioDAO {
	
	void crear(UsuarioDominio usuario);
	void actualizar(UsuarioDominio usuario);
	void eliminar(UsuarioDominio usuario);
	List<UsuarioDominio> consultar(UsuarioDominio usuario);
	
}