package com.pilae.pilaeTorneo.datos.dao.interfaces;

import java.util.List;

import com.pilae.pilaetorneo.dominio.PatrocinadorDominio;

public interface IPatrocinadorDAO {
	void crear(PatrocinadorDominio patrocinador);
	void actualizar(PatrocinadorDominio patrocinador);
	void eliminar(PatrocinadorDominio patrocinador);
	List<PatrocinadorDominio> consultar(PatrocinadorDominio patrocinador);
}
